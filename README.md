# HMM Project Documentation #

### Papers on HMM ###

* [Lawrence R. Rabiner: A Tutorial on Hidden Markov Models and Selected Applications in Speech Recognition. Proceedings of the IEEE, 1989, pages 257-286](http://www.cs.ubc.ca/~murphyk/Bayes/rabiner.pdf)
* [Jeff A. Bilmes: A Gentle Tutorial of the EM Algorithm and its Application to Parameter Estimation for Gaussian Mixture and Hidden Markov Models. International Computer Science Institute, Berkeley CA 1998](http://melodi.ee.washington.edu/people/bilmes/mypapers/em.pdf)
* [Sean R Eddy: What is a hidden Markov model? Nature Biotechnology 22, 1315 - 1316 (2004)](http://www.nature.com/nbt/journal/v22/n10/pdf/nbt1004-1315.pdf)

### How do I compile the HMM project ###

* [See wiki page](https://bitbucket.org/tstibor/lad2014/wiki/HMM%20compilation)

### How do I ###
* [Create a HMM](https://bitbucket.org/tstibor/lad2014/wiki/HMM%20usage)
* [Print a HMM](https://bitbucket.org/tstibor/lad2014/wiki/HMM%20usage)
* [Train a HMM](https://bitbucket.org/tstibor/lad2014/wiki/HMM%20usage)
* [Sample from a HMM](https://bitbucket.org/tstibor/lad2014/wiki/HMM%20usage)

### Example ###
* [Weather Example](https://bitbucket.org/tstibor/lad2014/wiki/HMM%20Weather%20Example)

### Visualization ###
* [HMM Emission Matrix of Lustre Logs Function Calls](https://bitbucket.org/tstibor/lad2014/wiki/HMM%20Visualization%20of%20Lustre%20Log%20Function%20Calls)
