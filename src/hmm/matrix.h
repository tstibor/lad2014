/* 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>. 
 */

/*
 * Copyright (c) 2014, Thomas Stibor <thomas@stibor.net>
 */

#ifndef MATRIX_H
#define MATRIX_H

/* Matrix row-major order: offset = row * NUMCOLS + column. */
#define MAT(A, row, col) A.data[row * A.ncol + col]

/* 3D matrix row-major order: offset = x + (y * A.dim_x) + (z * A.dim_y * A.dim_x). */
#define MAT3(A, row, col, ndepth) A.data[row + (col * A.nrow) + (ndepth * A.ncol * A.nrow)]

/* 2D Matrix. */
typedef struct {
    unsigned int nrow;
    unsigned int ncol;
    double *data;		/* row-major access via macro MAT */
} matrix_t;

/* 3D Matrix. */
typedef struct {
    unsigned int nrow;
    unsigned int ncol;
    unsigned int ndepth;
    double *data;		/* row-major access via macro MAT3D */
} matrix3_t;

/* Generate random seed and random matrix entries. */
unsigned long int rand_seed();
void unif_rand_mat(matrix_t *mat);

/* 2D Matrix functions. */
int alloc_mat(matrix_t *mat, const unsigned int nrow, const unsigned int ncol);
void free_mat(matrix_t *mat);
void row_stochastic_mat(matrix_t *mat);
void get_row(const matrix_t *mat, const unsigned int row, double *vec);
void get_col(const matrix_t *mat, const unsigned int col, double *vec);
void zero_mat(matrix_t *mat);
void print_mat(const matrix_t *mat);

/* 3D Matrix functions. */
int alloc_mat3(matrix3_t *mat,
	       const unsigned int nrow, 
	       const unsigned int ncol,
	       const unsigned int ndepth);
void free_mat3(matrix3_t *mat);
void print_mat3(const matrix3_t *mat);

#endif

