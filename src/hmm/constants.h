#ifndef CONSTANTS_H
#define CONSTANTS_H

#define VERSION 0.2

#define SUCCESS 0
#define ERROR 1
#define MAX_LEN_STATE_NAME 128	/* Max length of hidden and observable state names. */

#define ERR_MSG(msg)							\
    { printf("ERROR: %s, line %d, func %s(...), call %s(...) \"%s\"\n", \
	     __FILE__, __LINE__, __func__, msg, strerror(errno));	\
    };
#define ERR_MSG_RET(msg) { ERR_MSG(msg); return ERROR; };

#define MAGIC_MHMM 0x71		/* First byte in hmm file to represent multinomial hmm. */
#define MAGIC_GHMM 0x72		/* First byte in hmm file to represent gaussian hmm. */

#endif
