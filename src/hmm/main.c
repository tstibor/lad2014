/* 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>. 
 */

/*
 * Copyright (c) 2014, Thomas Stibor <thomas@stibor.net>
 */

#define _BSD_SOURCE		/* For strdup() */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <getopt.h>
#include <unistd.h>
#include <sys/stat.h>
#include <errno.h>
#include "matrix.h"
#include "hmm.h"
#include "io.h"
#include "constants.h"

static unsigned char param_create  = 0;
static unsigned char param_print   = 0;
static unsigned char param_sample  = 0;
static unsigned char param_train   = 0;
static unsigned char param_prob    = 0;
static unsigned char param_viterbi = 0;
static unsigned int arg_hidden     = 0;
static unsigned int arg_emission   = 0;
static unsigned int arg_seq_len    = 0;
static unsigned int arg_maxiter    = 0;
static char* arg_hmm_in_file       = NULL;   
static char* arg_hmm_out_file      = NULL;
static char* arg_data_in_file      = NULL;

/* TODO: Move that to io.c */
unsigned char file_exists(const char *file_name)
{
    struct stat buffer;
    int status;
    
    status = stat(file_name, &buffer);
    return (!!(status == 0));
}

void usage(const char *prg_name)
{
    printf("usage: %s\n", prg_name);
    printf("create HMM\n" \
	   "       -c, --create\n"			\
	   "              create a randomly initialized HMM\n" \
	   "       -o, --outfile=FILE\n" \
	   "              file name of stored HMM\n" \
	   "       -h, --hidden=NUM\n" \
	   "              number of hidden states\n" \
	   "       -e, --emission=NUM\n" \
	   "              number of emission states\n" \
	   "sample from HMM\n" \
	   "       -s, --sample\n" \
	   "              sample from HMM\n" \
	   "       -l, --length=NUM\n" \
	   "              length of the sequence to be sampled\n" \
	   "       -i, --infile=FILE\n" \
	   "              file name of HMM to read\n" \
	   "print HMM\n" \
	   "       -p, --print\n" \
	   "              print start vector, hidden and emission matrix of HMM\n" \
	   "       -i, --infile=FILE\n" \
	   "              file name of HMM to read\n" \
	   "train HMM\n" \
	   "       -t, --train\n" \
	   "              train HMM with Baum-Welch algorithm\n" \
	   "       -i, --infile=FILE\n" \
	   "              file name of HMM to read\n" \
	   "       -d, --datafile=FILE\n" \
	   "              data of sequences to be trained\n" \
	   "       -m, --maxiter=NUM\n" \
	   "              maximum number of iterations\n" \
	   "       -o, --outfile=FILE\n" \
	   "              file name of stored HMM\n" \
	   "log probability of sequences\n" \
	   "       -r, --prob\n" \
	   "            calculate log probability of sequence(s)\n" \
	   "       -i, --infile=FILE\n" \
	   "              file name of HMM to read\n" \
	   "       -d, --datafile=FILE\n" \
	   "              data of sequences to be trained\n" \
	   "viterbi algorithm\n" \
	   "       -v, --viterbi\n" \
	   "              determine most likely hidden states\n" \
	   "       -i, --infile=FILE\n" \
	   "              file name of HMM to read\n" \
	   "       -d, --datafile=FILE\n" \
	   "              data of sequences to be trained\n" \
	   "\nversion %2.2f © Thomas Stibor <thomas@stibor.net>\n", VERSION);

    exit(SUCCESS);
}

int main(int argc, char *argv[])
{
    if (argc == 1)
	usage(argv[0]);

    int c;
     
    while (1) {
	static struct option long_options[] = {
	    /* These options don't set a flag.
	       We distinguish them by their indices. */
	    {"create",    no_argument,       0, 'c'},
	    {"print",     no_argument,       0, 'p'},
	    {"sample",    no_argument,       0, 's'},
	    {"train",     no_argument,       0, 't'},
	    {"prob",      no_argument,       0, 'r'},
	    {"viterbi",   no_argument,       0, 'v'},
	    {"hidden",    required_argument, 0, 'h'},
	    {"emission",  required_argument, 0, 'e'},
	    {"outfile",   required_argument, 0, 'o'},
	    {"infile",    required_argument, 0, 'i'},
	    {"length",    required_argument, 0, 'l'},
	    {"maxiter",   required_argument, 0, 'm'},
	    {"datafile",  required_argument, 0, 'd'},
	    {0,           0,                 0,   0}
	};
	/* getopt_long stores the option index here. */
	int option_index = 0;
     
	c = getopt_long(argc, argv, "cpstrvh:e:o:i:l:m:d:",
			long_options, &option_index);
     
	/* Detect the end of the options. */
	if (c == -1)
	    break;
     
	switch (c) {
	case 0:
	    /* If this option set a flag, do nothing else now. */
	    if (long_options[option_index].flag != 0)
		break;
	    printf ("option %s", long_options[option_index].name);
	    if (optarg)
		printf (" with arg %s", optarg);
	    printf ("\n");
	    break;
     
	case 'c':
	    param_create = 1;
	    break;
     
	case 'p':
	    param_print = 1;
	    break;

	case 's':
	    param_sample = 1;
	    break;

	case 't':
	    param_train = 1;
	    break;

	case 'r':
	    param_prob = 1;
	    break;
	case 'v':
	    param_viterbi = 1;
	    break;
	    
	case 'h':
	    arg_hidden = atoi(optarg);
	    break;
     
	case 'e':
	    arg_emission = atoi(optarg);
	    break;
     
	case 'o':
	    arg_hmm_out_file = strdup(optarg);
	    break;

	case 'i':
	    arg_hmm_in_file = strdup(optarg);
	    break;

	case 'd':
	    arg_data_in_file = strdup(optarg);
	    break;

	case 'l':
	    arg_seq_len = atoi(optarg);
	    break;

	case 'm':
	    arg_maxiter = atoi(optarg);
	    break;
     
	case '?':
	    /* getopt_long already printed an error message. */
	    break;
     
	default:
	    abort ();
	}
    }

    if (optind < argc) {
	printf("non-option ARGV-elements: ");
	while (optind < argc)
	    printf("%s ", argv[optind++]);
	putchar('\n');
    }

    /*** Create HMM. ***/
    if (param_create && arg_hidden && arg_emission && arg_hmm_out_file) {
	hmm_t hmm;
	int rc;

	rc = alloc_rand_hmm(&hmm, arg_hidden, arg_emission);
	if (rc == ERROR) {
	    printf("cannot allocate memory for hmm with parameters\n");
	    printf("hidden: %d and emission: %d\n", arg_hidden, arg_emission);
	    
	    return ERROR;
	}
	rc = file_exists(arg_hmm_out_file);
	if (rc == 1) {
	    printf("file \"%s\" already exists, are you sure you want to override the file [y|n]? ", arg_hmm_out_file);
	    char c_in;
	    rc = scanf("%c", &c_in);
	    if (rc != 1)
		ERR_MSG_RET("scanf");
	    if (c_in != 'y') {
		printf("no hmm is created\n");
		free_hmm(&hmm);
		return ERROR;
	    }
	}

	rc = write_hmm(arg_hmm_out_file, &hmm);
	if (rc == ERROR) {
	    printf("cannot write hmm with parameters ");
	    printf("hidden: %d, emission: %d and file name: %s\n", arg_hidden, 
		   arg_emission, arg_hmm_out_file);
	    free_hmm(&hmm);
	
	    return ERROR;
	} 
	printf("successfully create hmm with parameters ");
	printf("hidden: %d, emission: %d and file name: %s\n", arg_hidden,
	       arg_emission, arg_hmm_out_file);
	free_hmm(&hmm);
	
	return SUCCESS;
    } /*** Sample from a HMM. ***/
    else if (param_sample && arg_seq_len && arg_hmm_in_file) {
	hmm_t hmm;
	int rc;

	rc = read_hmm(arg_hmm_in_file, &hmm);
	if (rc == ERROR) {
	    printf("cannot read hmm file \"%s\"\n", arg_hmm_in_file);
	    return ERROR;
	}
	seq_t seq_H, seq_E;
	rc = alloc_seq(&seq_H, arg_seq_len);
	if (rc == ERROR) {
	    free_hmm(&hmm);
	    return ERROR;
	}
	rc = alloc_seq(&seq_E, arg_seq_len);
	if (rc == ERROR) {
	    free_hmm(&hmm);
	    free_seq(&seq_H);
	    return ERROR;
	}
	sample_seq(&hmm, arg_seq_len, &seq_H, &seq_E);
	for (unsigned int l = 0; l < arg_seq_len; l++)
	    printf("%d->%d\t%s->%s\n", 
		   seq_H.O[l], seq_E.O[l], seq_H.sym_names[l], seq_E.sym_names[l]);
	free_seq(&seq_H);
	free_seq(&seq_E);
	free_hmm(&hmm);
	
	 return SUCCESS;
    } /*** Print HMM. ***/
    else if (param_print && arg_hmm_in_file) {
	hmm_t hmm;
	int rc;

	rc = read_hmm(arg_hmm_in_file, &hmm);
	if (rc == ERROR) {
	    printf("cannot read hmm file \"%s\"\n", arg_hmm_in_file);
	    return ERROR;
	}
	print_hmm(&hmm);
	free_hmm(&hmm);

	return SUCCESS;
    } /*** Train HMM. ***/
    else if (param_train && arg_hmm_in_file && arg_data_in_file 
	     && arg_maxiter && arg_hmm_out_file) {
	hmm_t hmm;
	int rc;

	rc = read_hmm(arg_hmm_in_file, &hmm);
	if (rc == ERROR) {
	    printf("cannot read hmm file \"%s\"\n", arg_hmm_in_file);
	    return ERROR;
	}
	seqs_t seqs;
	rc = load_seqs(arg_data_in_file, &seqs, &hmm);
	if (rc == ERROR) {
	    printf("cannot process sequence data file \"%s\"\n", arg_data_in_file);
	    free_hmm(&hmm);
	    return ERROR;
	}
	
	rc = baum_welch_estimate_multi_seq(&hmm, &seqs, arg_maxiter);
	if (rc == ERROR) {
	    printf("cannot run Baum-Welch algorithm\n");
	    free_hmm(&hmm);
	    free_seqs(&seqs);
	    return ERROR;
	}
	rc = file_exists(arg_hmm_out_file);
	if (rc == 1) {
	    printf("file \"%s\" already exists, are you sure you want to override the file [y|n]? ", arg_hmm_out_file);
	    char c_in;
	    rc = scanf("%c", &c_in);
	    if (rc != 1)
		ERR_MSG_RET("scanf");
	    if (c_in != 'y') {
		printf("no hmm is created\n");
		free_hmm(&hmm);
		return ERROR;
	    }
	}
	rc = write_hmm(arg_hmm_out_file, &hmm);
	if (rc == ERROR) {
	    printf("cannot write hmm with parameters ");
	    printf("hidden: %d, emission: %d and file name: %s\n", arg_hidden, 
		   arg_emission, arg_hmm_out_file);
	    free_hmm(&hmm);
	    free_seqs(&seqs);

	    return ERROR;
	} 
	printf("successfully trained hmm\n");

	free_hmm(&hmm);
	free_seqs(&seqs);
	return SUCCESS;
	
    } /*** Probability of sequences. ***/
    else if (param_prob && arg_hmm_in_file && arg_data_in_file) {
	hmm_t hmm;
	int rc;
	double log_prob = 0;
	double total_log_prob = 0;

	rc = read_hmm(arg_hmm_in_file, &hmm);
	if (rc == ERROR) {
	    printf("cannot read hmm file \"%s\"\n", arg_hmm_in_file);
	    return ERROR;
	}
	seqs_t seqs;
	rc = load_seqs(arg_data_in_file, &seqs, NULL);
	if (rc == ERROR) {
	    printf("cannot read sequence data file \"%s\"\n", arg_data_in_file);
	    free_hmm(&hmm);
	    return ERROR;
	}
	for (unsigned int k = 0; k < seqs.n_s; k++) {
	    log_prob = log_prob_seq(&hmm, (&seqs)->s[k]);
	    total_log_prob += log_prob;
	    printf("%d: %f\n", k, log_prob);
	}
	printf("sum: %f\n", total_log_prob);

	free_hmm(&hmm);
	free_seqs(&seqs);
	
	return SUCCESS;
    } /* Viterbi algorithm to determine the most likely hidden states. */
    else if (param_viterbi && arg_hmm_in_file && arg_data_in_file) {
	hmm_t hmm;
	seq_t best_hidden_seq;
	int rc;
	double log_prob = 0;

	rc = read_hmm(arg_hmm_in_file, &hmm);
	if (rc == ERROR) {
	    printf("cannot read hmm file \"%s\"\n", arg_hmm_in_file);
	    return ERROR;
	}
	seqs_t seqs;
	rc = load_seqs(arg_data_in_file, &seqs, NULL);
	if (rc == ERROR) {
	    printf("cannot read sequence data file \"%s\"\n", arg_data_in_file);
	    free_hmm(&hmm);
	    return ERROR;
	}

	/* Calculate Viterbi path of each sequence. */
	for (unsigned int k = 0; k < seqs.n_s; k++) {

	    /* Allocate memory for Viterbi path (hidden sequence). */
	    rc = alloc_seq(&best_hidden_seq, (&seqs)->s[k]->T);
	    if (rc == ERROR) {
		free_hmm(&hmm);
		free_seqs(&seqs);
		return ERROR;
	    }
	    
	    rc = viterbi(&hmm, (&seqs)->s[k], &best_hidden_seq, &log_prob);
	    if (rc == ERROR) {
		printf("ERROR: cannot execute viterbi algorithm\n");
		free_hmm(&hmm);
		free_seqs(&seqs);
		free_seq(&best_hidden_seq);
		return ERROR;
	    }
	    printf("seq: %d max joint log prob: %f\n", k, log_prob);
	    for (unsigned int t = 0; t < best_hidden_seq.T-1; t++)
		printf("%d:%s ", best_hidden_seq.O[t], hmm.states.h[best_hidden_seq.O[t]]);
	    printf("%d:%s\n", best_hidden_seq.O[best_hidden_seq.T-1], hmm.states.h[best_hidden_seq.O[best_hidden_seq.T-1]]);
	    
	    free_seq(&best_hidden_seq);
	}
	free_hmm(&hmm);
	free_seqs(&seqs);
	
	return SUCCESS;
    }
    else {
	printf("missing or incorrect parameter\n");
	usage(argv[0]);
    }
     
    return SUCCESS;
}

