/* 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>. 
 */

/*
 * Copyright (c) 2014, Thomas Stibor <thomas@stibor.net>
 */

#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <string.h>
#include <errno.h>
#include <sys/time.h>
#include <gsl/gsl_rng.h>
#include <gsl/gsl_randist.h>
#include "constants.h"
#include "matrix.h"

/* Return random seed value for gsl_ran_* functions.
 * If /dev/urandom provides random seed it is used,
 * otherwise current time secs and usecs are used.
 * 
 * @return random seed value, if seed value is correctly obtain, otherwise exit(1).
 */
unsigned long int rand_seed()
{
    unsigned int seed;
    FILE *devrandom;
    size_t total_read;

    devrandom = fopen("/dev/urandom", "r");
    if (devrandom  == NULL) {
	struct timeval tv;
	gettimeofday(&tv,0);
	seed = tv.tv_sec + tv.tv_usec;
    } else {
	total_read = fread(&seed, sizeof(seed), 1, devrandom);
	if (total_read != 1) {
	    ERR_MSG("fread");
	    fclose(devrandom);
	    exit(1);  /* Radically exit program if we get this error,
			 otherwise we get a poor seed. */
	}
	fclose(devrandom);
    }
    return(seed);
}

/* Matrix memory allocation.
 *
 * @param[in] mat Matrix for which memory to be allocated.
 * @param[in] nrow Number of rows.
 * @param[in] ncol Number of columns.
 * @param[out] mat Memory allocated matrix.
 * @return ERROR if allocation fails, otherwise SUCCESS.
 */
int alloc_mat(matrix_t *mat, const unsigned int nrow, const unsigned int ncol)
{
    mat->nrow = nrow;
    mat->ncol = ncol;
    /* Initialize with zeros. */
    mat->data = (double *)calloc(nrow * ncol, sizeof(double));

    if (mat->data == NULL)
	ERR_MSG_RET("calloc");
    return SUCCESS;
}

/* Matrix memory free-ing.
 *
 * @param[in] mat Matrix for which memory to be free-ed.
 */
void free_mat(matrix_t *mat)
{
    free(mat->data);
}

/* Make matrix entries uniform random numbers in the range [0.0, 1.0).
 *
 * @param[in] mat Matrix.
 * @param[out] mat Matrix.
 */
void unif_rand_mat(matrix_t *mat)
{
    gsl_rng *rng;

    gsl_rng_env_setup();
    rng = gsl_rng_alloc(gsl_rng_default);
    gsl_rng_set(rng, rand_seed());

    for (unsigned int r = 0; r < mat->nrow; r++)
	for (unsigned int c = 0; c < mat->ncol; c++)
	    MAT((*mat), r, c) = gsl_rng_uniform(rng);
    
    gsl_rng_free(rng);
}

/* Make matrix row stochastic, such that each row sums to 1.
 *
 * @param[in] mat Matrix to be row stochastic.
 * @param[out] mat Row stochastic matrix.
 */
void row_stochastic_mat(matrix_t *mat)
{
    for (unsigned int r = 0; r < mat->nrow; r++) {
	double sum = 0;
	for (unsigned int c = 0; c < mat->ncol; c++)
	    sum += MAT((*mat), r, c);
	for (unsigned int c = 0; c < mat->ncol; c++)
	    MAT((*mat), r, c) /= sum; /* Normalize row to sum to 1. */
    }
}

void get_row(const matrix_t *mat, const unsigned int row, 
	     double *vec)
{
    if (mat == NULL || vec == NULL)
	return;

    for (unsigned int c = 0; c < mat->ncol; c++) {
	vec[c] = MAT((*mat), row, c);
    }
}

void get_col(const matrix_t *mat, const unsigned int col, double *vec)
{
    if (mat == NULL || vec == NULL)
	return;

    for (unsigned int r = 0; r < mat->nrow; r++) {
	vec[r] = MAT((*mat), r, col);
    }
}

/* Set all entries in matrix to 0.
 *
 * @param[in] mat Matrix to be set to 0.
 */
void zero_mat(matrix_t *mat)
{
    memset(mat->data, 0, sizeof(double) * mat->nrow * mat->ncol);
}

/* Print matrix values.
 *
 * @param[in] mat Matrix to be printed.
 */
void print_mat(const matrix_t *mat)
{
    for (unsigned int r = 0; r < mat->nrow; r++) {
	for (unsigned int c = 0; c < mat->ncol; c++) {
	    printf("%f", MAT((*mat), r, c));
	    if (c != mat->ncol - 1)
		printf(" ");
	    else
		printf("\n");
	}
    }
}

int alloc_mat3(matrix3_t *mat,
	       const unsigned int nrow, 
	       const unsigned int ncol,
	       const unsigned int ndepth)
{
    mat->nrow = nrow;
    mat->ncol = ncol;
    mat->ndepth = ndepth;
    /* Initialize with zeros. */
    mat->data = (double *)calloc(nrow * ncol * ndepth, sizeof(double));

    if (mat->data == NULL)
	ERR_MSG_RET("calloc");
    return SUCCESS;
}

/* 3D Matrix memory free-ing.
 *
 * @param[in] mat 3D matrix for which memory to be free-ed.
 */
void free_mat3(matrix3_t *mat)
{
    free(mat->data);
}

/* Print 3D matrix values.
 *
 * @param[in] mat 3D matrix to be printed.
 */
void print_mat3(const matrix3_t *mat)
{
    for (unsigned int d = 0; d < mat->ndepth; d++) {
	printf("depth: %d\n", d);
	for (unsigned int r = 0; r < mat->nrow; r++)
	    for (unsigned int c = 0; c < mat->ncol; c++) {
		printf("%f", MAT3((*mat), r, c, d));
		if (c != mat->ncol - 1)
		    printf(" ");
		else
		    printf("\n");
	    }
    }
}
