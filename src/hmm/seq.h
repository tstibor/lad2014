/* 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>. 
 */

/*
 * Copyright (c) 2014, Thomas Stibor <thomas@stibor.net>
 */

#ifndef SEQ_H
#define SEQ_H

typedef struct {
    char **sym_names;		/* Pointer array of symbolic names in sequence. */
    unsigned int *O;		/* Array of non-negative integers representing sequence. */
    unsigned int T;		/* Total number of tokens (strings) in sequence (length of seq.). */
} seq_t;

typedef struct {
    seq_t **s;			/* Pointer to sequence s[0], s[1], ..., s[n_s-1]. */
    unsigned int n_s;		/* Total number of sequences. */
} seqs_t;

/* Allocate memory of sequence of length len. */
int alloc_seq(seq_t *seq, const unsigned int len);

/* Free memory of sequence. */
void free_seq(seq_t *seq);


#endif
