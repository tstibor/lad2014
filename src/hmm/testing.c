#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include "matrix.h"
#include "hmm.h"
#include "constants.h"
#include "io.h"

#define PASSED 0
#define FAILED 1

static unsigned int next_row = 0;

/* Recursive helper function to generate all possible state combinations. */
void rec_gen_states(unsigned int tmp_array[], const unsigned int array_size,
		    const unsigned int idx, const unsigned int n_states, 
		    unsigned int nQ, unsigned int seq_len, unsigned int Q[nQ][seq_len])
{
    if (idx == array_size) {
#ifdef DEBUG
        printf("\n");
        for(unsigned int i = 0; i < array_size; i++)
	    printf("%i ", tmp_array[i]);
#endif
	for(unsigned int i = 0; i < array_size; i++)
	    Q[next_row][i] = tmp_array[i];
	next_row++;
    }
    else 
	for (unsigned int i = 0; i < n_states; i++) {
	    tmp_array[idx] = i;
	    rec_gen_states(tmp_array, array_size, idx + 1, n_states, nQ, seq_len, Q);
    }
}

/* Generate all possible state combinations of certain length. */
void gen_all_states(const unsigned int nQ, 
		    const unsigned int seq_len, 
		    const unsigned int n_states, 
		    unsigned int Q[nQ][seq_len])
{
    unsigned int array[seq_len];
    rec_gen_states(array, seq_len, 0, n_states, nQ, seq_len, Q);
    next_row = 0;
}

void print_result(const unsigned char rc,
		  const char *text)
{
    rc == FAILED ? printf("FAILED %s\n", text) :
	printf("PASSED %s\n", text);

}

void dump_seqs(const seqs_t *seqs)
{
    printf("\n\nnumber of sequences: %d\n", seqs->n_s);
    for (unsigned int n = 0; n < seqs->n_s; n++) {
    	printf("sequence: %d of length: %d\n", n, seqs->s[n]->T);
    	for (unsigned int i = 0; i < seqs->s[n]->T; i++)
    	    printf("%s:%d ", seqs->s[n]->sym_names[i], seqs->s[n]->O[i]);
	printf("\n");
    }
}

/*** Matrix tests ****/
/* Verify that alloc_mat initialize matrix with zeros. */
unsigned char init_zero_test(const unsigned int nrow,
			     const unsigned int ncol)
{
    matrix_t mat;
    int rc;
    rc = alloc_mat(&mat, nrow, ncol);    
    if (rc == ERROR)
	return FAILED;

    for (unsigned int r = 0; r < mat.nrow; r++)
	for (unsigned int c = 0; c < mat.ncol; c++)
	    if (MAT(mat, r, c) != 0) {
		free_mat(&mat);
		return FAILED;
	    }
    free_mat(&mat);
    return PASSED;
}

/* Verify that zero_mat set all entries to zero. */
unsigned char set_zero_test(const unsigned int nrow,
			    const unsigned int ncol)
{
    matrix_t mat;
    int rc;
    rc = alloc_mat(&mat, nrow, ncol);
    if (rc == ERROR)
	return FAILED;

    for (unsigned int r = 0; r < mat.nrow; r++)
	for (unsigned int c = 0; c < mat.ncol; c++)
	    MAT(mat, r, c) = 1.0;

    zero_mat(&mat);

    for (unsigned int r = 0; r < mat.nrow; r++)
	for (unsigned int c = 0; c < mat.ncol; c++)
	    if (MAT(mat, r, c) != 0) {
		free_mat(&mat);
		return FAILED;
	    }
    free_mat(&mat);
    return PASSED;
}

/* Verify that row_stochastic_mat(...) makes each row sums to 1. */
unsigned char row_stochastic_test(const unsigned int nrow,
				  const unsigned int ncol)
{
    const float eps = 1e-9;
    matrix_t mat;
    int rc;
    rc = alloc_mat(&mat, nrow, ncol);    
    if (rc == ERROR)
	return FAILED;

    unif_rand_mat(&mat);
    row_stochastic_mat(&mat);	/* Make matrix row stochastic. */

    for (unsigned int r = 0; r < mat.nrow; r++) {
	double sum = 0;
	for (unsigned int c = 0; c < mat.ncol; c++)
	    sum += MAT(mat, r, c);
	if (sum < 1.0 - eps || sum > 1.0 + eps) { /* Does row sum to 1? */
	    free_mat(&mat);
	    return FAILED;
	}
    }
    free_mat(&mat);
    return PASSED;
}

/* Verify that get_row and get_col return the vector of desired row, column resp. */
unsigned char get_rowcol_vec_test()
{
    double i = 1.0;
    matrix_t A;
    int rc;

    rc = alloc_mat(&A, 3, 3);
    if (rc == ERROR)
	return FAILED;

    for (unsigned int r = 0; r < A.nrow; r++)
	for (unsigned int c = 0; c < A.ncol; c++)
	    MAT(A, r, c) = i++;

    double row_1[] = {4, 5, 6};
    double col_2[] = {3, 6, 9};
    double check_row[3];
    double check_col[3];
    
    get_row(&A, 1, check_row);
    get_col(&A, 2, check_col);

    free_mat(&A);

    for (unsigned int i = 0; i < 3; i++) {
	if (check_row[i] != row_1[i] ||
	    check_col[i] != col_2[i])
	    return FAILED;
    }
    return PASSED;
}

/*** 3D Matrix tests ***/
/* Verify that alloc_mat3 initialize matrix with zeros. */
unsigned char zero_test_3d(const unsigned int nrow,
			   const unsigned int ncol,
			   const unsigned int ndepth)
{
    matrix3_t mat;
    int rc;
    rc = alloc_mat3(&mat, nrow, ncol, ndepth);
    if (rc == ERROR)
	return FAILED;

    for (unsigned int r = 0; r < mat.nrow; r++)
	for (unsigned int c = 0; c < mat.ncol; c++)
	    for (unsigned int d = 0; d < mat.ndepth; d++)
		if (MAT3(mat, r, c, d) != 0) {
		    free_mat3(&mat);
		    return FAILED;
		}
    free_mat3(&mat);
    return PASSED;
}

/* Verify that 3D matrix z-dimension is correctly handled. */
unsigned char zero_test_1_3d(const unsigned int nrow,
			     const unsigned int ncol)
{
    matrix3_t mat;
    int rc;
    rc = alloc_mat3(&mat, nrow, ncol, 3);
    if (rc == ERROR)
	return FAILED;

    double v = 1;
    for (unsigned int r = 0; r < mat.nrow; r++)
	for (unsigned int c = 0; c < mat.ncol; c++) {
	    MAT3(mat, r, c, 0) = v;
	    MAT3(mat, r, c, 1) = -v;
	    MAT3(mat, r, c, 2) = v*v;
	    v++;
	}

    /* print_mat3(&mat); */ /* uncomment for visual inspection. */

    for (unsigned int r = 0; r < mat.nrow; r++)
	for (unsigned int c = 0; c < mat.ncol; c++) {
	    if (MAT3(mat, r, c, 0) * abs(MAT3(mat, r, c, 1)) != MAT3(mat, r, c, 2)) {
		free_mat3(&mat);
		return FAILED;
	    }
	}

    free_mat3(&mat);
    return PASSED;
}

/*** HMM tests ***/
/* Verify that sampling from a HMM works correctly. */
unsigned char hmm_sampling_1(const unsigned int s_len)
{

    hmm_t hmm;
    unsigned int n_h = 1;	  /* 1 hidden state. */
    unsigned int n_o = 2;	  /* 2 observable states. */
    seq_t seq_H;
    seq_t seq_E;
    int rc;

    /* Create HMM with 1 hidden state and
       2 observable states, where the first observable states
       has probability 0 be emitted. */
    rc = alloc_rand_hmm(&hmm, n_h, n_o);
    if (rc == ERROR)
    	return FAILED;
    MAT(hmm.E, 0, 0) = 0.0;
    MAT(hmm.E, 0, 1) = 1.0;

    rc = alloc_seq(&seq_H, s_len);
    if (rc == ERROR) {
	free_hmm(&hmm);
	return FAILED;
    }
    rc = alloc_seq(&seq_E, s_len);
    if (rc == ERROR) {
	free_hmm(&hmm);
	free_seq(&seq_H);
	return FAILED;
    }

    sample_seq(&hmm, s_len, &seq_H, &seq_E);
    for (unsigned int l = 0; l < s_len; l++) {
	if (seq_H.O[l] != 0 ||	/* We can sample only a 0, nothing else. */
	    seq_E.O[l] != 1) {	/* We can only sample a 1, nothing else.  */
	    free_seq(&seq_H);
	    free_seq(&seq_E);
	    free_hmm(&hmm);
	    return FAILED;
    	}
    }

    return PASSED;
}

unsigned char hmm_sampling_2(const unsigned int s_len)
{
    hmm_t hmm;
    unsigned int n_h = 1;	  /* 1 hidden state. */
    unsigned int n_o = 2;	  /* 2 observable states. */
    seq_t seq_H;
    seq_t seq_E;
    states_t state_names;

    int rc;

    /* Create HMM with 1 hidden state and
       2 observable states, where the second observable state
       has probability 0 be emitted. */
    rc = alloc_rand_hmm(&hmm, n_h, n_o);
    if (rc == ERROR)
    	return FAILED;
    MAT(hmm.E, 0, 0) = 1.0;
    MAT(hmm.E, 0, 1) = 0.0;

    rc = alloc_seq(&seq_H, s_len);
    if (rc == ERROR) {
	free_hmm(&hmm);
	return FAILED;
    }
    rc = alloc_seq(&seq_E, s_len);
    if (rc == ERROR) {
	free_hmm(&hmm);
	free_seq(&seq_H);
	return FAILED;
    }

    /* Create custom names (hidden:a, observable:A) */
    rc = alloc_states(&state_names, n_h, n_o);
    if (rc == ERROR) {
	free_hmm(&hmm);
	free_seq(&seq_H);
	free_seq(&seq_E);
    	return (FAILED);
    }
    for (unsigned int l = 0; l < hmm.states.n_h ; l++) {
    	char c_h[2] = {97 + l, '\0'};	/* 'a' = 97 */
    	strcpy(state_names.h[l], c_h);
    }
    for (unsigned int l = 0; l < hmm.states.n_o ; l++) {
    	char c_o[2] = {65 + l, '\0'};	/* 'A' = 65 */
    	strcpy(state_names.o[l], c_o);
    }
    set_state_names_hmm(&hmm, &state_names);

    sample_seq(&hmm, s_len, &seq_H, &seq_E);
    for (unsigned int l = 0; l < s_len; l++) {
    	if (strcmp(seq_H.sym_names[l], "a") != 0 ||
    	    strcmp(seq_E.sym_names[l], "A") != 0) {

	    free_hmm(&hmm);
	    free_seq(&seq_H);
	    free_seq(&seq_E);
	    free_states(&state_names);
    	    return FAILED;
    	}
    }
    free_hmm(&hmm);
    free_seq(&seq_H);
    free_seq(&seq_E);
    free_states(&state_names);

    return PASSED;
}

unsigned char hmm_sampling_3(const unsigned int s_len)
{
    hmm_t hmm;
    unsigned int n_h = 1;	  /* 1 hidden state. */
    unsigned int n_o = 3;	  /* 3 observable states. */
    seq_t seq_H;		  /* Sampled sequence of hidden states. */
    seq_t seq_E;		  /* Sampled sequence of emitted states. */
    unsigned int c[3] = {0, 0, 0}; /* Counting how often event occured. */
    const float eps = 1e-2;
    float desired = 1.0 / 3.0;
    int rc;

    /* Create HMM with 1 hidden state and
       3 observable states, where each observable state
       has probability 1/3 be emitted. Then check whether
       emitted distribution is around 1/3,1/3,1/3. */

    rc = alloc_rand_hmm(&hmm, n_h, n_o);
    if (rc == ERROR)
    	return FAILED;
    MAT(hmm.E, 0, 0) = desired;
    MAT(hmm.E, 0, 1) = desired;
    MAT(hmm.E, 0, 2) = desired;

    rc = alloc_seq(&seq_H, s_len);
    if (rc == ERROR) {
	free_hmm(&hmm);
	return ERROR;
    }
    rc = alloc_seq(&seq_E, s_len);
    if (rc == ERROR) {
	free_hmm(&hmm);
	free_seq(&seq_H);
	return ERROR;
    }

    sample_seq(&hmm, s_len, &seq_H, &seq_E);

    for (unsigned int l = 0; l < s_len; l++) {

	if (seq_E.O[l] == 0)
	    c[0] += 1;
	else if (seq_E.O[l] == 1)
	    c[1] += 1;
	else if (seq_E.O[l] == 2)
	    c[2] += 1;
	else {			/* We should never be here. */
	    goto failed;
	}
    }

    float sum = c[0] + c[1] + c[2];
    for (unsigned i = 0; i < 3; i++) {
	if (c[i]/sum < desired - eps ||
	    c[i]/sum > desired + eps)
	    goto failed;
    }

    free_seq(&seq_H);
    free_seq(&seq_E);
    free_hmm(&hmm);
    return PASSED;;
    
failed:
    free_seq(&seq_H);
    free_seq(&seq_E);
    free_hmm(&hmm);
    return FAILED;
}

int setup_weather_example(hmm_t *hmm)
{
    if (hmm == NULL)
	return FAILED;

    int rc;
    states_t state_names;

    /* Our standard weather example. */
    MAT(hmm->s, 0, 0) = 0.6; MAT(hmm->s, 0, 1) = 0.4;

    MAT(hmm->H, 0, 0) = 0.7; MAT(hmm->H, 0, 1) = 0.3;
    MAT(hmm->H, 1, 0) = 0.4; MAT(hmm->H, 1, 1) = 0.6;
    
    MAT(hmm->E, 0, 0) = 0.1; MAT(hmm->E, 0, 1) = 0.4; MAT(hmm->E, 0, 2) = 0.5;
    MAT(hmm->E, 1, 0) = 0.7; MAT(hmm->E, 1, 1) = 0.2; MAT(hmm->E, 1, 2) = 0.1;

    /* Create custom names (hidden: rainy, sunny, observable: walk, shop, clean) */
    rc = alloc_states(&state_names, 2, 3);
    if (rc == ERROR)
    	return FAILED;

    strcpy(state_names.h[0], "rainy");
    strcpy(state_names.h[1], "sunny");
    
    strcpy(state_names.o[0], "walk");
    strcpy(state_names.o[1], "shop");
    strcpy(state_names.o[2], "clean");

    set_state_names_hmm(hmm, &state_names);
    free_states(&state_names);

    return PASSED;
}

unsigned char hmm_forward_1()
{
    const unsigned n_h = 2;
    const unsigned n_o = 2;
    const unsigned seq_len = 3;
    const unsigned int n_allQ = (unsigned int)pow(n_h, seq_len);
    const unsigned int n_allO = (unsigned int)pow(n_o, seq_len);

    hmm_t hmm;
    int rc;
    double prob = 0;
    unsigned int H[n_allQ][seq_len];
    unsigned int O[n_allO][seq_len];
    
    rc = alloc_rand_hmm(&hmm, n_h, n_o);
    if (rc == ERROR)
    	return FAILED;

    gen_all_states(n_allQ, seq_len, n_h, H);
    gen_all_states(n_allO, seq_len, n_o, O);

    double prob_sum = 0;
    for (unsigned int k = 0; k < n_allO; k++) {
	for (unsigned int i = 0; i < n_allQ; i++) {
	    prob = 0;
	    prob = MAT(hmm.s, 0, H[i][0]) * MAT(hmm.E, H[i][0], O[k][0]);
	    for (unsigned int j = 0; j < seq_len - 1; j++) {
		prob *= MAT(hmm.H, H[i][j], H[i][j+1]) * MAT(hmm.E, H[i][j+1], O[k][j+1]);
	    }
	    printf("Pr(H = ");
	    for (unsigned int j = 0; j < seq_len; j++)
		printf("%d", H[i][j]);
	    printf(" | O = ");
	    for (unsigned int j = 0; j < seq_len; j++)
		printf("%d", O[k][j]);
	    printf(") = %f\n", prob);
	    prob_sum += prob;
	}
    }
    printf("sum prob: %f\n", prob_sum);

    free_hmm(&hmm);
    return PASSED;
}

unsigned char hmm_forward_2()
{
    const unsigned n_h = 2;
    const unsigned n_o = 3;
    const unsigned seq_len = 4;
    const unsigned int n_allQ = (unsigned int)pow(n_h, seq_len);
    const unsigned int n_allO = (unsigned int)pow(n_o, seq_len);

    hmm_t hmm;
    int rc;
    double prob = 0;
    unsigned int H[n_allQ][seq_len];
    unsigned int O[n_allO][seq_len];
    
    rc = alloc_rand_hmm(&hmm, n_h, n_o);
    if (rc == ERROR)
    	return FAILED;

    gen_all_states(n_allQ, seq_len, n_h, H);
    gen_all_states(n_allO, seq_len, n_o, O);

    /* Our standard weather example. 
       Make sure H is 2x2 and E 2x3 matrix.*/
    setup_weather_example(&hmm);

    seq_t seq_alpha_pass;
    seq_alpha_pass.O = malloc(sizeof(unsigned int) * seq_len);
    seq_alpha_pass.T = seq_len;

    double prob_sum = 0;
    double prob_seq_check = 0;
    /* Iterate over all input sequences of length seq_len. */
    for (unsigned int s = 0; s < n_allO; s++) {
	/* Iterate over all states. */
	for (unsigned int q = 0; q < n_allQ; q++) {
	    prob = 0;
	    prob = MAT(hmm.s, 0, H[q][0]) * MAT(hmm.E, H[q][0], O[s][0]);
	    for (unsigned int j = 0; j < seq_len - 1; j++) {
		prob *= MAT(hmm.H, H[q][j], H[q][j+1]) * MAT(hmm.E, H[q][j+1], O[s][j+1]);
	    }

	    printf("Pr(H = ");
	    for (unsigned int j = 0; j < seq_len; j++)
	    	printf("%d", H[q][j]);
	    printf(" | O = ");
	    for (unsigned int j = 0; j < seq_len; j++)
	    	printf("%d", O[s][j]);
	    printf(") = %f\n", prob);

	    prob_sum += prob;
	    prob_seq_check += prob;
	}
	printf("prob of seq: ");
	for (unsigned int k = 0; k < seq_len; k++)
	    printf("%d", O[s][k]);
	printf(" = %f\n", prob_seq_check);

	/* alpha_pass result must match with prob_seq. */
	printf("alpha_pass of seq: ");
	for (unsigned int k = 0; k < seq_len; k++) {
	    printf("%d", O[s][k]);
	    seq_alpha_pass.O[k] = O[s][k];
	}
	printf(" == %f\n", exp(log_prob_seq(&hmm, &seq_alpha_pass)));
	prob_seq_check = 0;
    }
    printf("sum prob: %f\n", prob_sum);

    free(seq_alpha_pass.O);
    free_hmm(&hmm);
    return PASSED;
}

unsigned char hmm_forward_backward()
{
    const unsigned n_h = 2;
    const unsigned n_o = 3;

    hmm_t hmm;
    matrix_t alpha;
    matrix_t beta;
    int rc;
    
    rc = alloc_rand_hmm(&hmm, n_h, n_o);
    if (rc == ERROR)
    	return FAILED;

    /* Our standard weather example. 
       Make sure H is 2x2 and E 2x3 matrix.*/
    setup_weather_example(&hmm);

    seq_t seq_weather;
    rc = alloc_seq(&seq_weather, 4); /* 0 1 0 2 */
    if (rc == ERROR) {
	free_hmm(&hmm);
	return ERROR;
    }
    seq_weather.O[0] = 0;
    seq_weather.O[1] = 1;
    seq_weather.O[2] = 0;
    seq_weather.O[3] = 2;

    rc = alloc_mat(&alpha, 4, n_h);
    if (rc == ERROR) {
	free_hmm(&hmm);
	free_seq(&seq_weather);
	return ERROR;
    }
    rc = alloc_mat(&beta, 4, n_h);
    if (rc == ERROR) {
	free_hmm(&hmm);
	free_seq(&seq_weather);
	free_mat(&alpha);
	return ERROR;
    }

    printf("alpha matrix weather with observation 0 1 0 2\n");
    alpha_pass_unscaled(&hmm, &seq_weather, &alpha);
    print_mat(&alpha);
    printf("\nbeta matrix weather with observation 0 1 0 2\n");
    beta_pass_unscaled(&hmm, &seq_weather, &beta);
    print_mat(&beta);

    free_hmm(&hmm);
    free_seq(&seq_weather);
    free_mat(&alpha);
    free_mat(&beta);

    return SUCCESS;
}
    
unsigned char hmm_weather()
{
    hmm_t hmm;
    int rc;
    const char *filename = "2_3_weather.hmm";

    rc = alloc_rand_hmm(&hmm, 2, 3);
    if (rc == ERROR)
    	return FAILED;

    setup_weather_example(&hmm);
    print_hmm(&hmm);
    rc = write_hmm(filename, &hmm);
    if (rc == ERROR) {
	printf("cannot write hmm weather example %s\n", filename);
	free_hmm(&hmm);
	return FAILED;
    }

    free_hmm(&hmm);
    return PASSED;
}

int main()
{
    printf("*** 2d matrix tests: ***\n");
    print_result(init_zero_test(3, 3), "init_zero_test 3x3");
    print_result(init_zero_test(30, 40), "init_zero_test 30x40");

    print_result(set_zero_test(3, 3), "set_zero_test 3x3");
    print_result(set_zero_test(30, 40), "set_zero_test 30x40");

    print_result(row_stochastic_test(4, 6), "row_stochastic_test 4x6");
    print_result(row_stochastic_test(20, 50), "row_stochastic_test 20x50");
    print_result(get_rowcol_vec_test(), "get_rowcol_vec_test");

    printf("\n*** 3d matrix tests: ***\n");
    print_result(zero_test_3d(3,3,3), "zero_test_3d 3x3x3");
    print_result(zero_test_3d(30,40,50), "zero_test_3d 30x40x50");
    print_result(zero_test_1_3d(2,2), "v,-v,v*v test 2x2x3"); /* TODO: To be removed. */

    printf("\n*** saving/loading hmm tests: ***\n");
    print_result(hmm_weather(), "hmm_weather()");

    printf("\n*** hmm sampling tests: ***\n");
    print_result(hmm_sampling_1(10), "hmm_sampling_1");
    print_result(hmm_sampling_2(10), "hmm_sampling_2");
    print_result(hmm_sampling_3(1e6), "hmm_sampling_3");

    printf("\nhmm forward tests:\n");
    print_result(hmm_forward_1(), "hmm_forward_1()");
    print_result(hmm_forward_2(), "hmm_forward_2()");
    print_result(hmm_forward_backward(), "hmm_forward_backward()");
    
    return 0;
}
