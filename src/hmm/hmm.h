/* 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>. 
 */

/*
 * Copyright (c) 2014, Thomas Stibor <thomas@stibor.net>
 */

#ifndef HMM_H
#define HMM_H

#include "seq.h"

/* Hidden and observable state names. */
typedef struct {
    char **h;			/* Array of hidden state names. */
    char **o;			/* Array of obserable state names. */
    unsigned int n_h;		/* Number of hidden state names (must match with T.nrow and T.ncol). */
    unsigned int n_o;		/* Number of observable state names (must match with E.ncol). */
} states_t;

/* Hidden Markov model. */
typedef struct {
    matrix_t s;		/* Start probability vector. */
    matrix_t H;		/* Transition probability matrix of hidden states. */
    matrix_t E;		/* Emission probability matrix of obserable states.*/
    states_t states;	/* Number and names of hidden and observable states. */
} hmm_t;

/* Allocate memory of HMM. */
int alloc_rand_hmm(hmm_t *hmm, const unsigned int n_h,
		   unsigned int const n_o);
/* Free memory of HMM. */
void free_hmm(hmm_t *hmm);

/* Allocate memory of state names of HMM. */
int alloc_states(states_t *states, const unsigned int n_h,
		 const unsigned int n_o);
/* Free memory of state names of HMM. */
void free_states(states_t *states);

/* Set string names of hidden and observable states. */
void set_state_names_hmm(hmm_t *hmm, const states_t *states);

/* Print all entities of of HMM (matrices, state names). */
void print_hmm(const hmm_t *hmm);

/* Sample hidden and observable sequence of HMM of length len. */
void sample_seq(const hmm_t *hmm, const const unsigned int len,
		seq_t *seq_H, seq_t *seq_O);

/* Train HMM with Baum-Welch algorithm of array of sequences. */
int baum_welch_estimate_multi_seq(hmm_t *hmm, const seqs_t *seqs, const unsigned int max_iter);

void alpha_pass(const hmm_t *hmm, const seq_t *seq, matrix_t *alpha, double *c);
void alpha_pass_unscaled(const hmm_t *hmm, const seq_t *seq, matrix_t *alpha);

void beta_pass(const hmm_t *hmm, const seq_t *seq, const double *c, matrix_t *beta);
void beta_pass_unscaled(const hmm_t *hmm, const seq_t *seq, matrix_t *beta);

double log_prob_seq(const hmm_t *hmm, const seq_t *seq);

int viterbi(const hmm_t *hmm, const seq_t *seq, seq_t *best_seq, double *log_prob);
int posterior_decoding(const hmm_t *hmm, const seq_t *seq, seq_t *best_seq, double *log_prob);

#endif
