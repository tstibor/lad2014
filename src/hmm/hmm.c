/* 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>. 
 */

/*
 * Copyright (c) 2014, Thomas Stibor <thomas@stibor.net>
 * 
 * Implementation is based on following papers:
 *
 * 1.) A Tutorial on Hidden Markov Models and Selected Applications in Speech Recognition.
 *     Lawrence R. Rabiner, Proceedings of the IEEE, Vol. 77, No. 2, February 1989.
 *     (http://www.cs.ubc.ca/~murphyk/Bayes/rabiner.pdf).
 * 2.) A Revealing Introduction to Hidden Markov Models.
 *     Mark Stamp (http://www.cs.sjsu.edu/faculty/stamp/RUA/HMM.pdf).
 */

#include <stdio.h>
#include <time.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>
#include <float.h>
#include <math.h>
#include <error.h>
#include <gsl/gsl_rng.h>
#include <gsl/gsl_randist.h>
#include "constants.h"
#include "matrix.h"
#include "hmm.h"
#include "seq.h"

/* Allocate memory and initialize randomly HMM matrices.
 *
 * @param[in] hmm Hidden Markov model to be initialized.
 * @param[in] n_h Number of hidden states.
 * @param[in] n_o Number of observable states.
 * @param[out] hmm Initialized Hidden Markov model.
 * @return ERROR if allocation fails, otherwise SUCCESS.
 */
int alloc_rand_hmm(hmm_t *hmm, const unsigned int n_h,
		   unsigned int const n_o)
{
    gsl_rng *rng;
    int rc;

    rc = alloc_mat(&hmm->s, 1, n_h);
    if (rc == ERROR)
	return rc;
    rc = alloc_mat(&hmm->H, n_h, n_h);
    if (rc == ERROR) {
	goto free_mat_s;
    }
    rc = alloc_mat(&hmm->E, n_h, n_o);
    if (rc == ERROR) {
	goto free_mat_s_H;
    }

    hmm->states.n_h = n_h;
    hmm->states.n_o = n_o;

    gsl_rng_env_setup();
    rng = gsl_rng_alloc(gsl_rng_default);
    gsl_rng_set(rng, rand_seed());
    const float sigma_h = 0.01; /* TODO: proportional to n_h, e.g. powf(n_h, -sqrt(n_h)) */
    const float sigma_o = 0.01; /* TODO: proportional to o_h. */

    /* Initialize entries in matrices approx. as follows:
     * hmm->s[i] = 1/n_h, hmm->H[i,j] = 1/n_h and hmm->E[i,j] = 1/n_o 
     */
    for (unsigned int i = 0; i < n_h; i++) {
	MAT(hmm->s, 0, i) =  fabs(1/(float)n_h + gsl_ran_gaussian(rng, sigma_h));
	for (unsigned int j = 0; j < n_h; j++)
	    MAT(hmm->H, i, j) = fabs(1/(float)n_h + gsl_ran_gaussian(rng, sigma_h));
	for (unsigned int k = 0; k < n_o; k++)
	    MAT(hmm->E, i, k) = fabs(1/(float)n_o + gsl_ran_gaussian(rng, sigma_o));
    }
    gsl_rng_free(rng);

    row_stochastic_mat(&hmm->s);	/* Create row stochastic start vector. */
    row_stochastic_mat(&hmm->H);	/* Create row stochastic transition matrix. */
    row_stochastic_mat(&hmm->E);	/* Create row stochastic emission matrix. */

    hmm->states.h = malloc(sizeof(char *) * hmm->H.nrow);
    if (hmm->states.h == NULL) {
	ERR_MSG("malloc");
	goto free_mat_s_H_E;
    }
    hmm->states.o = malloc(sizeof(char *) * hmm->E.ncol);
    if (hmm->states.o == NULL) {
	ERR_MSG("malloc");
	free(hmm->states.h);
	goto free_mat_s_H_E;
    }

    for (unsigned int r = 0; r < hmm->H.nrow; r++) {
	hmm->states.h[r] = malloc(sizeof(char) * MAX_LEN_STATE_NAME);
	if (hmm->states.h[r] == NULL) {
	    ERR_MSG("malloc");
	    for (unsigned int k = 0; k < r; k++)
		free(hmm->states.h[r]);
	    free(hmm->states.h);
	    goto free_mat_s_H_E;
	}
	sprintf(hmm->states.h[r], "h%d", r);
    }

    for (unsigned c = 0; c < hmm->E.ncol; c++) {
	hmm->states.o[c] = malloc(sizeof(char) * MAX_LEN_STATE_NAME);
	if (hmm->states.o[c] == NULL) {
	    ERR_MSG("malloc");
	    for (unsigned int k = 0; k < c; k++)
		free(hmm->states.o[c]);
	    free(hmm->states.h);
	    free(hmm->states.o);
	    goto free_mat_s_H_E;
	}
	sprintf(hmm->states.o[c], "o%d", c);
    }

    rc = SUCCESS;
    return rc;

free_mat_s_H_E: 
    free_mat(&hmm->E);
free_mat_s_H: 
    free_mat(&hmm->H);
free_mat_s: 
    free_mat(&hmm->s);

    rc = ERROR;
    return rc;
}

void print_hmm(const hmm_t *hmm)
{
    printf("hidden states: %d\n", hmm->H.nrow);
    for (unsigned int i = 0; i < hmm->H.nrow; i++) {
	printf("%s ", hmm->states.h[i]);
    }
    printf("\nobservable states: %d\n", hmm->E.ncol);
    for (unsigned int i = 0; i < hmm->E.ncol; i++) {
	printf("%s ", hmm->states.o[i]);
    }
    printf("\nstart prob:\n");
    print_mat(&hmm->s);
    printf("transition matrix:\n");
    print_mat(&hmm->H);
    printf("emission matrix:\n");
    print_mat(&hmm->E);
}

void free_hmm(hmm_t *hmm)
{
    free_mat(&hmm->s);
    free_mat(&hmm->H);
    free_mat(&hmm->E);
    for (unsigned int r = 0; r < hmm->H.nrow; r++)
	free(hmm->states.h[r]);
    free(hmm->states.h);
    for (unsigned c = 0; c < hmm->E.ncol; c++)
	free(hmm->states.o[c]);
    free(hmm->states.o);
}

unsigned int get_state(const unsigned int *result, const unsigned int len)
{
    unsigned int i;
    for (i = 0; i < len; i++)
	if (result[i] == 1)
	    return i;
    /* We should never end up here. */
    assert(i == len);
    return ERROR;
}


void sample_seq(const hmm_t *hmm, const unsigned int len,
		seq_t *seq_H, seq_t *seq_O)
{
    double row_pT[hmm->H.ncol];
    double row_pE[hmm->E.ncol];
    unsigned int result_T[hmm->H.ncol];
    unsigned int result_E[hmm->E.ncol];
    unsigned int h_state;
    unsigned int o_state;
    gsl_rng *rng;

    if (hmm == NULL || len == 0 || seq_H == NULL || seq_O == NULL)
	return;
    if (seq_H->T != len || seq_O->T != len)
	return;

    gsl_rng_env_setup();
    rng = gsl_rng_alloc(gsl_rng_default);
    gsl_rng_set(rng, rand_seed()); /* rand_seed(); or rand() same seed. */

    /* Pick row from T to obtain probablity vector row_pT. */
    get_row(&(hmm->s), 0, row_pT);

    for (unsigned int l = 0; l < len; l++) {

	/* Sample from Multinomial distribution and store in result_T. */
	gsl_ran_multinomial(rng, hmm->H.ncol, 1, row_pT, result_T); 
	/* Obtain hidden state from result_T. */
	h_state = get_state(result_T, hmm->H.ncol);

	/* Pick row from E to obtain probablity vector row_pE. */
	get_row(&(hmm->E), h_state, row_pE);
	/* Sampe from Multinomial distribution and store in result_E. */
	gsl_ran_multinomial(rng, hmm->E.ncol, 1, row_pE, result_E); 
	/* Obtain observable state from result_E. */
	o_state = get_state(result_E, hmm->E.ncol);

	seq_H->O[l] =  h_state;
	seq_O->O[l] =  o_state;

	strcpy(seq_H->sym_names[l], hmm->states.h[h_state]);
	strcpy(seq_O->sym_names[l], hmm->states.o[o_state]);

	/* Start over, pick row from T to obtain probablity vector row_pT. */
	get_row(&(hmm->H), h_state, row_pT);
    }

    gsl_rng_free(rng);
}

int alloc_states(states_t *states, const unsigned int n_h,
		 const unsigned int n_o)
{
    states->n_h = n_h;
    states->n_o = n_o;

    states->h = malloc(sizeof(char *) * n_h);
    if (states->h == NULL)
	ERR_MSG_RET("malloc");

    states->o = malloc(sizeof(char *) * n_o);
    if (states->o == NULL) {
	ERR_MSG("malloc");
	free(states->h);
	return ERROR;
    }

    /* TODO: Optimize that in one for-loop. */
    for (unsigned int s = 0; s < n_o; s++) {
	states->o[s] = malloc(sizeof(char) * MAX_LEN_STATE_NAME);
	if (states->o[s] == NULL) {
	    ERR_MSG("malloc");
	    for (unsigned int k = 0; k < s; k++)
		free(states->o[s]);
	    free(states->h);
	    free(states->o);
	    return ERROR;
	}
    }
    for (unsigned int s = 0; s < n_h; s++) {
	states->h[s] = malloc(sizeof(char) * MAX_LEN_STATE_NAME);
	if (states->h[s] == NULL) {
	    ERR_MSG("malloc");
	    for (unsigned int k = 0; k < s; k++)
		free(states->h[s]);
	    for (unsigned int j = 0; j < n_o; j++)
		free(states->o[s]);
	    free(states->h);
	    free(states->o);
	    return ERROR;
	}
    }
    return SUCCESS;
}

void free_states(states_t *states)
{
    if (states == NULL)
	return;
    /* TODO: Optimize that in one for-loop. */
    for (unsigned int s = 0; s < states->n_h; s++) {
	free(states->h[s]);
    }
    for (unsigned int s = 0; s < states->n_o; s++) {
	free(states->o[s]);
    }
    free(states->o);
    free(states->h);
}

void set_state_names_hmm(hmm_t *hmm, const states_t *states)
{
    if (hmm->states.n_h != states->n_h) {
    	printf("warning: condition of hmm->states.n_h != states->n_h does not match\n");
    	return;
    }

    if (hmm->states.n_o != states->n_o) {
	printf("warning: condition of hmm->states.n_o != states->n_o does not match\n");
	return;
    }

    /* TODO: Optimize that in one for-loop. */
    for (unsigned int s = 0; s < states->n_h; s++) {
	strcpy(hmm->states.h[s], states->h[s]);
    }
    for (unsigned int s = 0; s < states->n_o; s++) {
	strcpy(hmm->states.o[s], states->o[s]);
    }
}

/* Problem 1: Calculate log probability of sequence given the model.
 *
 * @param[in] hmm Hidden Markov model.
 * @param[in] seq Input sequence.
 * @return 0 if calculation fails, otherwise log probability of sequence given the model.
 */
double log_prob_seq(const hmm_t *hmm, const seq_t *seq)
{
    matrix_t alpha;
    double c[seq->T];
    double prob = 0;
    int rc;

    rc = alloc_mat(&alpha, seq->T, hmm->H.ncol);
    if (rc == ERROR)
        return 0;

    alpha_pass(hmm, seq, &alpha, c);

    for (unsigned int t = 0; t < seq->T; t++)
	prob -= log(c[t]);

    free_mat(&alpha);
    return prob;
}

/* Baum-Welch alpha forward pass (with scaling)
 *
 * @param[in] hmm Hidden Markov model.
 * @param[in] seq Input sequence.
 * @param[out] alpha Matrix of probabilities of dim T x n_h
 * @param[out] c scaling array of length seq->T required by beta_pass.
 */
void alpha_pass(const hmm_t *hmm, const seq_t *seq, 
		matrix_t *alpha, double *c)
{
    const unsigned int n_h = hmm->states.n_h;
    const unsigned int T = seq->T;

    /* Compute alpha_0(i) */
    c[0] = 0;
    for (unsigned int i = 0; i < n_h; i++) {
        MAT((*alpha), 0, i) = hmm->s.data[i] * MAT(hmm->E, i, (seq->O[0]));
    	c[0] += MAT((*alpha), 0, i);
    }
    c[0] = 1/c[0];

    /* Scale alpha_0(i) */
    if (c[0] != 0) {
        for (unsigned int i = 0; i < n_h; i++)
            MAT((*alpha), 0, i) = c[0] *  MAT((*alpha), 0, i);
    }

    /* Compute alpha_t(i) */
    for (unsigned int t = 1; t < T; t++) {
	c[t] = 0;
	for (unsigned int i = 0; i < n_h; i++) {
	    MAT((*alpha), t, i) = 0;
	    for (unsigned int j = 0; j < n_h; j++)
		MAT((*alpha), t, i) = MAT((*alpha), t, i) + MAT((*alpha), (t - 1), j) * MAT(hmm->H, j, i);
	    MAT((*alpha), t, i) = MAT((*alpha), t, i) * MAT(hmm->E, i, (seq->O[t]));
	    c[t] += MAT((*alpha), t, i);
	}
	c[t] = 1 / c[t];

	/* Scale alpha_t(i) */
	if (c[t] != 0) {
	    for (unsigned int i = 0; i < n_h; i++)
		MAT((*alpha), t, i) = c[t] * MAT((*alpha), t, i);
	}
    }
}

/* Baum-Welch alpha forward pass (without scaling)
 *
 * Note, this is more for testing purposes as for large T the values in
 * alpha quickly approaches zero.
 * @param[in] hmm Hidden Markov model.
 * @param[in] seq Input sequence.
 * @param[out] alpha Matrix of probabilities of dim T x n_h
 */
void alpha_pass_unscaled(const hmm_t *hmm, const seq_t *seq, matrix_t *alpha)
{
    unsigned int n_h = hmm->states.n_h;
    unsigned int T = seq->T;

    for (unsigned int i = 0; i < n_h; i++) {
        MAT((*alpha), 0, i) = hmm->s.data[i] * MAT(hmm->E, i, (seq->O[0]));
    }

    /* Compute alpha_t(i) */
    for (unsigned int t = 1; t < T; t++) {
	for (unsigned int i = 0; i < n_h; i++) {
	    MAT((*alpha), t, i) = 0;
	    for (unsigned int j = 0; j < n_h; j++)
		MAT((*alpha), t, i) = MAT((*alpha), t, i) + MAT((*alpha), (t - 1), j) * MAT(hmm->H, j, i);
	    MAT((*alpha), t, i) = MAT((*alpha), t, i) * MAT(hmm->E, i, (seq->O[t]));
	}
    }
}

/* Baum-Welch beta backward pass (with scaling)
 *
 * Note, this is more for testing purposes as for large T the values in
 * beta quickly approaches zero.
 * @param[in] hmm Hidden Markov model.
 * @param[in] seq Input sequence.
 * @param[in] c Scaling array of length seq->T required for beta_pass.
 * @param[out] beta Matrix of probabilities of dim T x n_h.
 */
void beta_pass(const hmm_t *hmm, const seq_t *seq, const double c[seq->T],
	       matrix_t *beta)
{
    unsigned int T = seq->T;
    unsigned int n_h = hmm->states.n_h;

    /* Scaling. */
    for (unsigned int i = 0; i < n_h; i++)
    	MAT((*beta), (T-1), i) = c[T-1];

    for (int t = T - 2; t >= 0; t--) {
	for (unsigned int i = 0; i < n_h; i++) {
	    MAT((*beta), t, i) = 0;
	    for (unsigned int j = 0; j < n_h; j++) {
		MAT((*beta), t, i) = MAT((*beta), t, i) + 
		    MAT(hmm->H, i, j) * MAT(hmm->E, j, (seq->O[t+1])) * MAT((*beta), (t+1), j);
	    }
	    /* Scale beta_t(i) with same scale factor as alpha_t(i). */
	    MAT((*beta), t, i) =  c[t] * MAT((*beta), t, i);
	}
    }
}

/* Baum-Welch beta backward pass (without scaling)
 *
 * @param[in] hmm Hidden Markov model.
 * @param[in] seq Input sequence.
 * @param[out] beta Matrix of probabilities of dim T x n_h.
 */
void beta_pass_unscaled(const hmm_t *hmm, const seq_t *seq, matrix_t *beta)
{
    unsigned int T = seq->T;
    unsigned int n_h = hmm->states.n_h;

    for (unsigned int i = 0; i < n_h; i++)
    	MAT((*beta), (T-1), i) = 1;

    for (int t = T - 2; t >= 0; t--) {
	for (unsigned int i = 0; i < n_h; i++) {
	    MAT((*beta), t, i) = 0;
	    for (unsigned int j = 0; j < n_h; j++) {
		MAT((*beta), t, i) = MAT((*beta), t, i) + 
		    MAT(hmm->H, i, j) * MAT(hmm->E, j, (seq->O[t+1])) * MAT((*beta), (t+1), j);
	    }
	}
    }
}

/* Baum-Welch gamma computation
 *
 * @param[in] hmm Hidden Markov model.
 * @param[in] seq Input sequence.
 * @param[in] alpha Matrix of probabilities of dim T x n_h
 * @param[in] beta Matrix of probabilities of dim T x n_h
 * @param[out] gamma Matrix of probabilities of dim 1 x n_h.
 * @param[out] di_gamma Matrix of probabilities of dim T x n_h x n_h.
 */
void compute_gamma(const hmm_t *hmm, const seq_t *seq, 
		   const matrix_t *alpha, const matrix_t *beta,
		   matrix_t *gamma, matrix3_t *di_gamma)
{
    unsigned int T = seq->T;
    unsigned int n_h = hmm->states.n_h;

    for (unsigned int t = 0; t < T - 1; t++) {
	double denom = 0;
	for (unsigned int i = 0; i < n_h; i++)
	    for (unsigned int j = 0; j < n_h; j++)
		denom = denom + 
		    MAT((*alpha), t, i) * MAT(hmm->H, i, j) *
		    MAT(hmm->E, j, (seq->O[t+1])) * MAT((*beta), (t+1), j); /* Denominator eq. 37 */

	for (unsigned int i = 0; i < n_h; i++) {
	    MAT((*gamma), t, i) = 0;
	    for (unsigned int j = 0; j < n_h; j++) {
		MAT3((*di_gamma), t, i, j) = (MAT((*alpha), t , i) * MAT(hmm->H, i, j) *
					      MAT(hmm->E, j, (seq->O[t+1])) * MAT((*beta), (t+1), j)) / denom;
		MAT((*gamma), t, i) = MAT((*gamma), t, i) + MAT3((*di_gamma), t, i, j);
	    }
	}
    }
}

/* Initialize matrices for Baum-Welch estimation.
 *
 * @param[in] alpha Uninitialized matrix.
 * @param[in] beta Uninitialized matrix.
 * @param[in] gamma Uninitialized matrix.
 * @param[in] di_gamma Uinitialized 3D matrix.
 * @param[in] T Length of sequence.
 * @param[in] n_h Number of hidden states of HMM.
 * @param[out] alpha Zero initialized matrix of dim T x n_h.
 * @param[out] beta Zero initialized matrix of dim T x n_h.
 * @param[out] gamma Zero initialized matrix of dim T x n_h.
 * @param[out] di_gamma Zero initialized matrix of dim T x n_h x n_h.
 * @return ERROR if initialization fails, otherwise SUCCESS.
 */
int init_baum_welch(matrix_t *alpha, matrix_t *beta,
		    matrix_t *gamma, matrix3_t *di_gamma,
		    const unsigned int T, const unsigned int n_h)
{
    int rc;
    
    rc = alloc_mat(alpha, T, n_h);
    if (rc == ERROR)
	return ERROR;

    rc = alloc_mat(beta, T, n_h);
    if (rc == ERROR)
	goto free_alpha;

    rc = alloc_mat(gamma, T, n_h);
    if (rc == ERROR)
	goto free_beta;

    rc = alloc_mat3(di_gamma, T, n_h, n_h);
    if (rc == ERROR)
	goto free_gamma;
    else
	return SUCCESS;

free_gamma:
    free_mat(gamma);

free_beta:
    free_mat(beta);

free_alpha:
    free_mat(alpha);
    
    return ERROR;
}

int alloc_multi_seq_matrices(matrix_t *sum_s,
			     matrix_t *sum_H_numer, matrix_t *sum_H_denom,
			     matrix_t *sum_E_numer, matrix_t *sum_E_denom,
			     const unsigned int n_h, const unsigned int n_o)
{
    int rc;

    rc = alloc_mat(sum_s, 1, n_h);
    if (rc == ERROR)
	return ERROR;

    rc = alloc_mat(sum_H_numer, n_h, n_h);
    if (rc == ERROR)
	goto free_s;

    rc = alloc_mat(sum_H_denom, n_h, n_h);
    if (rc == ERROR)
	goto free_H_numer;

    rc = alloc_mat(sum_E_numer, n_h, n_o);
    if (rc == ERROR)
	goto free_H_denom;

    rc = alloc_mat(sum_E_denom, n_h, n_o);
    if (rc == ERROR)
	goto free_E_numer;
    else
	return SUCCESS;

free_E_numer:
    free_mat(sum_E_numer);

free_H_denom:
    free_mat(sum_H_numer);

free_H_numer:
    free_mat(sum_H_numer);

free_s:
    free_mat(sum_s);

    return ERROR;
}

/* Baum-Welch estimation.
 *
 * @param[in] hmm Initialized hidden Markov model.
 * @param[in] seqs Observation sequences.
 * @param[in] max_iter Maximum number of iterations.
 * @param[out] hmm Estimated hidden Markov model for input sequence.
 * @return ERROR if estimation fails, otherwise SUCCESS.
 */
int baum_welch_estimate_multi_seq(hmm_t *hmm, const seqs_t *seqs, const unsigned int max_iter)
{
    if (hmm == NULL || seqs == NULL)
	return ERROR;

    unsigned int iters = 0;	   /* Current number of iterstions. */
    const double eps_diff = 1e-10; /* Termination criterion of abs(log_prob - prev_log_prob) <= eps_diff. */

    const unsigned int n_h = hmm->states.n_h; /* Number of hidden states. */
    const unsigned int n_o = hmm->states.n_o; /* Number of observable states. */
    int rc;
    
    double log_prob = 0;       /* Log probability of current iteration. */
    double prev_log_prob = 0;  /* Log probability of previous iteration. */
    double numer = 0;	       /* Numerator value in re-estimating H and E. */
    double denom = 0;	       /* Denomintor value in re-estimating H and E. */

    matrix_t sum_s;		/* Start vector sum over all sequences. */
    matrix_t sum_H_numer;	/* Numerator for H matrix update sum over all sequences. */
    matrix_t sum_H_denom;	/* Denominator for H matrix update sum over all sequences. */
    matrix_t sum_E_numer;	/* Numerator for E matrix update sum over all sequences. */
    matrix_t sum_E_denom;	/* Denominator for E matrix update sum over all sequences. */
    
    /* Allocate memory for the temporary matrices. */
    rc = alloc_multi_seq_matrices(&sum_s,
				  &sum_H_numer, &sum_H_denom,
				  &sum_E_numer, &sum_E_denom,
				  n_h, n_o);
    if (rc == ERROR)
	return ERROR;

    do { /* Iter while max_iter are reached or log_prob - prev_log_prob. */
	prev_log_prob = log_prob;
	log_prob = 0;
	/* For each sequence k provided in data structure seqs_t. */
	for (unsigned int k = 0; k < seqs->n_s; k++) {

	    const unsigned int T = seqs->s[k]->T;
	    double c[T];	/* Scaling vector for alpha and beta pass. */
	    matrix_t alpha;	/* Matrix for alpha pass. */
	    matrix_t beta;	/* Matrix for beta pass. */
	    matrix_t gamma;	/* Matrix for gamma computations. */
	    matrix3_t di_gamma;	/* 3D Matrix for di-gamma computations. */

	    /*** 1. Initialization. ***/
	    rc = init_baum_welch(&alpha, &beta, &gamma, &di_gamma, T, n_h);
	    if (rc == ERROR)
		return ERROR;

	    /*** 2. Alpha-pass. ***/
	    alpha_pass(hmm, seqs->s[k], &alpha, c);

	    /*** 3. Beta-pass. ***/
	    beta_pass(hmm, seqs->s[k], c, &beta);

	    /*** 4. Compute gamma_t(i,j) and gamma_t(i). ***/
	    compute_gamma(hmm, seqs->s[k], &alpha, &beta, &gamma, &di_gamma);

	    /* Sum over number of sequences k and later divide sum_s[i] / k. */
	    for (unsigned int i = 0; i < n_h; i++)
		MAT(sum_s, 0, i) += MAT(gamma, 0, i);

	    /* Sum over number of sequences k and later divide by gamma denominator. */
	    for (unsigned int i = 0; i < n_h; i++) {
		for (unsigned int j = 0; j < n_h; j++) {
		    numer = 0; denom = 0;
		    for (unsigned int t = 0; t < T - 1; t++) {
			numer += MAT3(di_gamma, t, i, j); /* Numerator eq. (40b) */
			denom += MAT(gamma, t, i);        /* Denominator eq. (40b) */
		    }
		    MAT(sum_H_numer, i, j) += numer;
		    MAT(sum_H_denom, i, j) += denom;
		}
	    }

	    /* Sum over number of sequences k and later divide by gamma denominator. */
	    for (unsigned int i = 0; i < n_h; i++) {
		for (unsigned int j = 0; j < n_o; j++) {
		    numer = 0; denom = 0;
		    for (unsigned int t = 0; t < T; t++) {
			if (seqs->s[k]->O[t] == j)
			    numer += MAT(gamma, t, i); /* Numerator eq. (40c) */
			denom += MAT(gamma, t, i);     /* Denominator eq. (40c) */
		    }
		    MAT(sum_E_numer, i, j) += numer;
		    MAT(sum_E_denom, i, j) += denom;
		}
	    }
	    
	    /* Calculate log probability of sequence k. */
	    for (unsigned int t = 0; t < T; t++) {
		log_prob -= log(c[t]);
	    }
	    /* Cleanup matrices. */
	    free_mat(&alpha);
	    free_mat(&beta);
	    free_mat(&gamma);
	    free_mat3(&di_gamma);
	} /* End-for iter over all sequences. */

	/* Finally average over all sequences and update HMM parameters. */
	for (unsigned int i = 0; i < n_h; i++)
	    MAT(hmm->s, 0, i) = MAT(sum_s, 0, i) / (double)seqs->n_s;

	for (unsigned int i = 0; i < n_h; i++)
	    for (unsigned int j = 0; j < n_h; j++) {
		/* TODO: */
		if (MAT(sum_H_denom, i, j) == 0) {
		    printf("ASSERT: MAT(sum_H_denom, i, j) == 0\n");
		}
		MAT(hmm->H, i, j) = MAT(sum_H_numer, i, j) / MAT(sum_H_denom, i, j);
	    }
	
	for (unsigned int i = 0; i < n_h; i++)
	    for (unsigned int j = 0; j < n_o; j++) {
		/* TODO: */
		if (MAT(sum_E_denom, i, j) == 0) {
		    printf("ASSERT: MAT(sum_E_denom, i, j) == 0\n");
		}
		MAT(hmm->E, i, j) = MAT(sum_E_numer, i, j) / MAT(sum_E_denom, i, j);
	    }

	printf("iter: %4d, log_prob: %f\n", iters, log_prob);
	iters++;
	zero_mat(&sum_s);
	zero_mat(&sum_H_numer);
	zero_mat(&sum_H_denom);
	zero_mat(&sum_E_numer);
	zero_mat(&sum_E_denom);

	/* We should get a strictly increasing series of log prob, that is,
	   log_prob > prev_log_prob at each iteration. If at some iteration,
	   log_prob <= prev_log_prob, then overfitting occured and thus we terminate learning. */
	if (log_prob <= prev_log_prob && iters > 1) {
	    printf("******************************************************************************\n");
	    printf("learning starts to overfit (reached local maxima) at iteration %d\n", iters - 1);
	    printf("******************************************************************************\n");
	    iters = max_iter;
	}
	if (fabs(log_prob - prev_log_prob) <= eps_diff) {
	    printf("******************************************************************************\n");
	    printf("learning terminates due to too small eps_diff improvement at iteration %d\n", iters - 1);
	    printf("******************************************************************************\n");
	    iters = max_iter;
	}
    } while (iters < max_iter);

    free_mat(&sum_s);
    free_mat(&sum_H_numer);
    free_mat(&sum_H_denom);
    free_mat(&sum_E_numer);
    free_mat(&sum_E_denom);

    return SUCCESS;
}

/* Viterbi algorithm to calculate the most probable hidden state path (sequence).
 *
 * @param[in] hmm Initialized hidden Markov model.
 * @param[in] seq Observation sequence.
 * @param[out] best_seq Most likely correspoding hidden state sequence.
 * @param[out] log_prob Max log joint probability P(q1 q2 ... qT, o1 o2 ... oT | HMM).
 * @return ERROR if Viterbi algorithm fails, otherwise SUCCESS.
 */
int viterbi(const hmm_t *hmm, const seq_t *seq, seq_t *best_seq, double *log_prob)
{
    int rc;
    matrix_t delta;
    matrix_t psi;

    unsigned int best_state;
    double value, min_value;

    if (hmm == NULL || seq == NULL || best_seq == NULL || log_prob == NULL) {
	printf("ERROR: null pointer detected: hmm %p seq %p best_seq %p log_prob %p\n",
	       (void *)hmm, (void *)seq, (void *)best_seq, (void *)log_prob);
	return ERROR;
    }
    /* Calculate the Viterbi path. */
    const unsigned int T = seq->T;

    rc = alloc_mat(&delta, T, hmm->H.ncol);
    if (rc == ERROR)
	return ERROR;

    rc = alloc_mat(&psi, T, hmm->H.ncol);
    if (rc == ERROR) {
	free_mat(&delta);
	return ERROR;
    }

    /* Values in matrix delta are log scaled. */
    /* 1.) Initialization. */
    for (unsigned int i = 0; i < hmm->states.n_h; i++) {
	MAT(delta, 0, i) = -log(hmm->s.data[i]) -log(MAT(hmm->E, i, (seq->O[0]))); /* Eq. 32a */
	MAT(psi, 0, i) = 0;	/* Eq. 32b */
    }
    min_value = 0;
    
    /* 2.) Recursion. */
    for (unsigned int t = 1; t < T; t++) {
	for (unsigned int j = 0; j < hmm->states.n_h; j++) {
	    best_state = 0;
	    min_value = MAT(delta, (t-1), 0) - log(MAT(hmm->H, 0, j));
	    for (unsigned int i = 1; i < hmm->states.n_h; i++) {
		value = MAT(delta, (t-1), i) - log(MAT(hmm->H, i, j)); /* max[delta_{t-1}(i)a_{ij}], 1 <= i <= N. */
		if (value < min_value) {
		    best_state = i; /* Remember the state, i.e. argmax[delta_{t-1}(i)a_{ij}], 1 <= i <= N. */
		    min_value = value;
		}
	    }
	    MAT(delta, t, j) = min_value - log(MAT(hmm->E, j, seq->O[t])); /* Eq. 33a. */
	    MAT(psi, t, j) = (unsigned int)best_state; /* Eq. 33b. */
	}
    }

    /* 3.) Termination. */
    best_state = 0;
    min_value = MAT(delta, (T-1), 0);
    for (unsigned int i = 1; i < hmm->states.n_h; i++) {
	if (MAT(delta, (T-1), i) < min_value) {
	    best_state = i;
	    min_value = MAT(delta, (T-1), i);
	}
    }
    best_seq->O[T-1] = best_state;
    /* Path backtracking. */
    for (int t = T - 2; t >= 0; t--) /* Note unsigned int, induces that t >= 0 is always true. */
	best_seq->O[t] = (unsigned int)MAT(psi, (t + 1), (best_seq->O[t+1])); /* Eq. 35. */
	
    *log_prob = -min_value;	/* Note: exp(-log_prob) gives prob. */

    free_mat(&delta);
    free_mat(&psi);

    return SUCCESS;
}

/* To be implemented. */
int posterior_decoding(const hmm_t *hmm, const seq_t *seq, seq_t *best_seq, double *log_prob)
{
    return ERROR;
}
