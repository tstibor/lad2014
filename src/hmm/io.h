/* 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>. 
 */

/*
 * Copyright (c) 2014, Thomas Stibor <thomas@stibor.net>
 */

#ifndef IO_H
#define IO_H

#include "seq.h"
#include "matrix.h"
#include "hmm.h"

int load_seqs(const char *file_name, seqs_t *seqs, hmm_t *hmm);
void free_seqs(seqs_t *seqs);
int read_hmm(const char *file_name, hmm_t *hmm);
int write_hmm(const char *file_name, const hmm_t *hmm);

#endif
