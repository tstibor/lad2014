/* 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>. 
 */

/*
 * Copyright (c) 2014, Thomas Stibor <thomas@stibor.net>
 */

#define _GNU_SOURCE
#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <string.h>
#include <errno.h>
#include <ctype.h>
#include <search.h>
#include "io.h"
#include "constants.h"

static void *root = NULL;	/* Pointer to root of binary tree. */
static const void *val = NULL;	/* Return pointer of tsearch() to check whether data is in tree. */
static unsigned int key = 0;	/* Integer key of string in binary tree. */

unsigned int n_o = 0;         /* Number of observable states. */
char **dict_sym_names = NULL; /* Dictionary of symbol names collected from data of sequences. */
unsigned int dict_size = 0;   /* Size of dictionary, will be determined in walker() function. */

typedef struct {
    char sym_name[MAX_LEN_STATE_NAME];
    unsigned int i;
} tree_item_t;

static int compare(const void *pa, const void *pb)
{
    const tree_item_t *item_a = (const tree_item_t *)pa;
    const tree_item_t *item_b = (const tree_item_t *)pb;

    return (strcmp(item_a->sym_name, item_b->sym_name));
}

void walker(const void *node, const VISIT which, const int depth)
{
    tree_item_t *tree_item = NULL;

    switch (which) {
    case preorder:
	tree_item = *(tree_item_t **)node;
	if (tree_item->i < n_o)
	    strcpy(dict_sym_names[tree_item->i], tree_item->sym_name);
	else
	    printf("WARNING: tree mismatch: n_o:%d <= tree_item->i:%d\n", n_o, tree_item->i);
	dict_size++;
	break;
    case postorder:
	break;
    case endorder:
	break;
    case leaf:
	tree_item = *(tree_item_t **)node;
	if (tree_item->i < n_o)
	    strcpy(dict_sym_names[tree_item->i], tree_item->sym_name);
	else
	    printf("WARNING: tree mismatch: n_o:%d <= tree_item->i:%d\n", n_o, tree_item->i);
	dict_size++;
	break;
    }
}

/* Parse line containing a sequence and put sequence into seq_t struct.
 *
 * @param[in] line Sequence separated by space or tab.
 * @param[in] symbol_seq Allocated struct to be filled.
 * @param[out] symbol_seq Allocated struct filled with sequence.
 * @return ERROR if allocation fails, otherwise SUCCESS.
 */
static int parse_line(const char *line, seq_t *seq)
{
    char seq_tok[MAX_LEN_STATE_NAME];
    unsigned int chr_cnt = 0;

    for (size_t i = 0; i < strlen(line); i++) {
    	if (line[i] == ' ' || line[i] == '\t' || line[i] == '\n') {
    	    seq_tok[chr_cnt] = '\0';
    	    seq->sym_names = realloc(seq->sym_names, sizeof(char *) *
				     (seq->T + 1));

    	    seq->sym_names[seq->T] = calloc(1, sizeof(char) * (chr_cnt + 1));
	    seq->O = realloc(seq->O, sizeof(unsigned int) * (seq->T + 1));
	    strncpy(seq->sym_names[seq->T], seq_tok, chr_cnt);

	    tree_item_t *tree_item = calloc(1, sizeof(tree_item_t)); /* Memory is free'd in load_seqs tdestroy(). */
	    strncpy(tree_item->sym_name, seq_tok, chr_cnt);
	    tree_item->i = key;

	    val = tfind(tree_item, &root, compare);
	    if (val == NULL) {
		val = tsearch(tree_item, &root, compare);
		key++;
	    } else
		free(tree_item); /* Item exists already in tree, thus release it. */
	    
	    seq->O[seq->T] = (*(tree_item_t **)val)->i;
    	    seq->T++;
    	    chr_cnt = 0;
    	}
    	else
    	    seq_tok[chr_cnt++] = line[i];
    }
    return SUCCESS;
}

/* Load sequences from file and put sequences into seqs_t struct.
 *
 * @param[in] file_name Name of file to read.
 * @param[in] seqs Allocated struct to be filled.
 * @param[out] seqs Allocated struct filled with sequences.
 * @return ERROR if allocation fails, otherwise SUCCESS.
 */
int load_seqs(const char *file_name, seqs_t *seqs, hmm_t *hmm)
{
    FILE *file;
    char *line = NULL;
    size_t len = 0;
    ssize_t read;
    int rc = SUCCESS;

    file = fopen(file_name, "r");
    if (file == NULL)
	ERR_MSG_RET("fopen");

    seqs->s = NULL; seqs->n_s = 0;

    while ((read = getline(&line, &len, file)) != -1) {

	if (strlen(line) > 0 && line[0] != '\n') {
	    seq_t *seq = calloc(1, sizeof(seq_t));
	    parse_line(line, seq);
	    seqs->s = realloc(seqs->s, sizeof(seq_t *) * (seqs->n_s + 1));
	    seqs->s[seqs->n_s++] = seq;
	}
    }

    /* Set hmm emission state names to sequence names stored in dictionary. */
    if (hmm != NULL) {
	n_o = hmm->states.n_o;
	dict_sym_names = malloc(sizeof(char *) * n_o);
	if (dict_sym_names == NULL) {
	    ERR_MSG("malloc");
	    rc = ERROR;
	    goto cleanup;
	}
	for (unsigned int i = 0; i < n_o; i++) {
	    dict_sym_names[i] = malloc(sizeof(char) * MAX_LEN_STATE_NAME);
	    if (dict_sym_names[i] == NULL) {
		ERR_MSG("malloc");
		for (unsigned int k = 0; k < i; k++) /* Release memory up the point we hit the malloc error. */
		    free(dict_sym_names[k]);
		free(dict_sym_names);
		rc = ERROR;
		goto cleanup;
	    }
	}

	/* Traverse tree and set in walker() function hmm emission state names to sym_name found in tree. */
	twalk(root, walker);

	/* Make sure number of emission states matches with number of unique names in data sequences. */
	if (dict_size == n_o) {
	    for (unsigned int i = 0; i < n_o; i++)
		strcpy(hmm->states.o[i], dict_sym_names[i]);
	} else {
	    printf("ASSERTION: number of emission states n_o does not match with "
		   "number of unique emission names found in sequence data, "
		   "n_o = %d != dict_size = %d\n", n_o, dict_size);
	    rc = ERROR;
	}

	for (unsigned int i = 0; i < n_o; i++)
	    free(dict_sym_names[i]);
	free(dict_sym_names);
    }

cleanup:
    if (line)
	free(line);
    if (root)
	tdestroy(root, free);

    return rc;
}

/* Free memory of stored sequences in struct.
 *
 * @param[in] seqs Allocated struct of sequences to be freed.
 */
void free_seqs(seqs_t *seqs)
{
    for (unsigned int n = 0; n < seqs->n_s; n++) {
	for (unsigned int tok = 0; tok < seqs->s[n]->T; tok++)
	    free(seqs->s[n]->sym_names[tok]);
	free(seqs->s[n]->sym_names);
	free(seqs->s[n]->O);
	free(seqs->s[n]);
    }
    free(seqs->s);
}

/* Read HMM from file.
 *
 * @param[in] file_name Filename of HMM to read.
 * @param[in] hmm HMM data structure to be filled with read data.
 *            HMM memory will be allocated and needs to be freed by the caller.
 * @param[out] hmm HMM filled with data read from file_name.
 * @return ERROR if read fails, otherwise SUCCESS.
 */
int read_hmm(const char *file_name, hmm_t *hmm)
{
    FILE *file;
    file = fopen(file_name, "rb");
    if (file == NULL)
	ERR_MSG_RET("fopen");

    size_t total_read;
    unsigned int n_h, n_o;
    unsigned char hmm_sig;

    /* Read magic signature for multinomial hmm. */
    total_read = fread(&hmm_sig, sizeof(unsigned char), 1, file);
    if (total_read != 1) goto failed;

    if (hmm_sig != MAGIC_MHMM) {
	printf("unknown hmm file format of file %s\n", file_name);
	errno = -1;
	goto failed;
    }

    /* Read number of hidden states (number of columns of start vector). */
    total_read = fread(&n_h, sizeof(unsigned int), 1, file);
    if (total_read != 1) goto failed;

    /* Read number of emission states (number of columns of emission matrix). */
    total_read = fread(&n_o, sizeof(unsigned int), 1, file);
    if (total_read != 1) goto failed;

    /* Setup a randomly initialized hmm and overwrite values in matrices and state names. */
    int rc;
    rc = alloc_rand_hmm(hmm, n_h, n_o);
    if (rc == ERROR) goto failed;

    /* Read start vector. */
    total_read = fread(hmm->s.data, sizeof(double), hmm->s.ncol, file);
    if (total_read != hmm->s.ncol) {
    	goto failed;
    }

    /* Read hidden matrix. */
    total_read = fread(hmm->H.data, sizeof(double), hmm->H.nrow * hmm->H.ncol, file);
    if (total_read != hmm->H.nrow * hmm->H.ncol) {
    	goto failed;
    }

    /* Read emission matrix. */
    total_read = fread(hmm->E.data, sizeof(double), hmm->E.nrow * hmm->E.ncol, file);
    if (total_read != hmm->E.nrow * hmm->E.ncol) {
    	goto failed;
    }

    unsigned char str_len;

    /* Read state names of hidden states. */
    for (unsigned int i = 0; i < hmm->states.n_h; i++) {
    	total_read = fread(&str_len, sizeof(unsigned char), 1, file);
	if (total_read != 1) goto failed;

    	total_read = fread(hmm->states.h[i], sizeof(char), str_len, file);
    	if (total_read != str_len) goto failed;

	(hmm->states.h[i])[str_len] = '\0';
    }

    /* Read state names of observable states. */
    for (unsigned int i = 0; i < hmm->states.n_o; i++) {
    	total_read = fread(&str_len, sizeof(unsigned char), 1, file);
	if (total_read != 1) goto failed;

    	total_read = fread(hmm->states.o[i], sizeof(char), str_len, file);
    	if (total_read != str_len) goto failed;

	(hmm->states.o[i])[str_len] = '\0';
    }

    fclose(file);
    return SUCCESS;

failed:
    ERR_MSG("fread");
    fclose(file);
    return ERROR;
}

/* Save HMM to file.
 *
 * @param[in] file_name Name of file to write.
 * @param[in] hmm Data of HMM to be saved (hmm needs to be preallocated).
 * @return ERROR if write fails, otherwise SUCCESS.
 */
int write_hmm(const char *file_name, const hmm_t *hmm)
{
    FILE *file;
    file = fopen(file_name, "wb");
    if (file == NULL)
	ERR_MSG_RET("fopen");

    size_t total_write;
    unsigned char hmm_sig = MAGIC_MHMM;

    /* Write magic signature for multinomial hmm. */
    total_write = fwrite(&hmm_sig, sizeof(unsigned char), 1, file);
    if (total_write != 1) goto failed;

    /* Write number of hidden states (number of columns of start vector). */
    total_write = fwrite(&hmm->s.ncol, sizeof(unsigned int), 1, file);
    if (total_write != 1) goto failed;

    /* Write number of emission states (number of columns of emission matrix). */
    total_write = fwrite(&hmm->E.ncol, sizeof(unsigned int), 1, file);
    if (total_write != 1) goto failed;

    /* Write start vector. */
    total_write = fwrite(hmm->s.data, sizeof(double), hmm->s.ncol, file);
    if (total_write != hmm->s.ncol) goto failed;

    /* Write hidden matrix. */
    total_write = fwrite(hmm->H.data, sizeof(double), hmm->H.nrow * hmm->H.ncol, file);
    if (total_write != hmm->H.nrow * hmm->H.ncol) goto failed;

    /* Write emission matrix. */
    total_write = fwrite(hmm->E.data, sizeof(double), hmm->E.nrow * hmm->E.ncol, file);
    if (total_write != hmm->E.nrow * hmm->E.ncol) goto failed;

    unsigned char str_len;

    /* Write state names of hidden states. */
    for (unsigned int i = 0; i < hmm->states.n_h; i++) {
	/* Write length of char array, so we know length of state names when reading hmm model. */
	str_len = strlen(hmm->states.h[i]);
	total_write = fwrite(&str_len, sizeof(unsigned char), 1, file);
	if (total_write != 1) goto failed;

	/* Write char array. */
	total_write = fwrite(hmm->states.h[i], sizeof(char), strlen(hmm->states.h[i]), file);
	if (total_write != strlen(hmm->states.h[i])) goto failed;
    }

    /* Write state names of observable states. */
    for (unsigned int i = 0; i < hmm->states.n_o; i++) {
	str_len = strlen(hmm->states.o[i]);
	total_write = fwrite(&str_len, sizeof(unsigned char), 1, file);
	if (total_write != 1) goto failed;

	total_write = fwrite(hmm->states.o[i], sizeof(char), strlen(hmm->states.o[i]), file);
	if (total_write != strlen(hmm->states.o[i])) goto failed;
    }

    fflush(file);
    fclose(file);
    return SUCCESS;

failed:
    ERR_MSG("fwrite");
    fclose(file);
    return ERROR;
}
