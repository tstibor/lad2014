/* 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>. 
 */

/*
 * Copyright (c) 2014, Thomas Stibor <thomas@stibor.net>
 */

#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <string.h>
#include "constants.h"
#include "seq.h"

/* Allocate memory for sequence of length T.
 *
 * @param[in] seq Sequence to be initialized, must be NULL.
 * @param[in] len Length of the sequence.
 * @param[out] seq Initialized sequence.
 * @return ERROR if allocation fails or already allocated, otherwise SUCCESS.
 */
int alloc_seq(seq_t *seq, const unsigned int T)
{
    seq->T = T;
    seq->O = malloc(sizeof(unsigned int) * T);
    if (seq->O == NULL)
	ERR_MSG_RET("malloc");

    seq->sym_names = malloc(sizeof(char *) * T);
    if (seq->sym_names == NULL) {
    	ERR_MSG("malloc");
    	free(seq->O);
    	seq->T = 0;
    	return ERROR;
    }
    for (unsigned int t = 0; t < seq->T; t++) {
    	seq->sym_names[t] = calloc(MAX_LEN_STATE_NAME, sizeof(char));
    	if (seq->sym_names[t] == NULL) {
    	    ERR_MSG("malloc");
    	    for (unsigned int k = 0; k < t; k++) /* Release memory up the point we hit the calloc error. */
    		free(seq->sym_names[k]);
    	    free(seq->sym_names);
    	    free(seq->O);
    	    seq->T = 0;
    	}
    }

    return SUCCESS;
}

/* Free memory of sequence.
 *
 * @param[in] seq Sequence to be free'd.
 */
void free_seq(seq_t *seq)
{
    seq->T = 0;
    free(seq->O);
    for (unsigned int t = 0; t < seq->T; t++) {
    	if (seq->sym_names[t] != NULL)
    	    free(seq->sym_names[t]);
    }
    free(seq->sym_names);
}

