\documentclass{beamer}
\usetheme{Singapore}
\usepackage{etex}
\usepackage[ngerman]{babel}
\usepackage[utf8]{inputenc}
\usepackage[T1]{fontenc}
\usepackage{graphicx}
\usepackage{pstricks,pst-all}
\usepackage{moresize}
\usepackage{textpos}
\usepackage{color}
\usepackage{xcolor}
\usepackage{psfrag}
\usepackage{amsmath}
\usepackage{listings}
\newcommand\Small{\fontsize{6}{6}\selectfont}
\newcommand*\LSTfont{\Small\ttfamily}
\hypersetup{%
  colorlinks=true,
  pdfpagemode=FullScreen
}

\definecolor{mygreen}{rgb}{0,0.6,0}
\definecolor{mygray}{rgb}{0.5,0.5,0.5}
\definecolor{mymauve}{rgb}{0.58,0,0.82}

\lstset{ %
  backgroundcolor=\color{white},   % choose the background color; you must add \usepackage{color} or \usepackage{xcolor}
  basicstyle=\footnotesize,        % the size of the fonts that are used for the code
  breakatwhitespace=false,         % sets if automatic breaks should only happen at whitespace
  breaklines=true,                 % sets automatic line breaking
  captionpos=b,                    % sets the caption-position to bottom
  commentstyle=\color{mygreen},    % comment style
  deletekeywords={...},            % if you want to delete keywords from the given language
  escapeinside={\%*}{*)},          % if you want to add LaTeX within your code
  extendedchars=true,              % lets you use non-ASCII characters; for 8-bits encodings only, does not work with UTF-8
  frame=single,                    % adds a frame around the code
  keepspaces=true,                 % keeps spaces in text, useful for keeping indentation of code (possibly needs columns=flexible)
  keywordstyle=\color{blue},       % keyword style
  language=Octave,                 % the language of the code
  morekeywords={*,...},            % if you want to add more keywords to the set
  numbers=none,                    % where to put the line-numbers; possible values are (none, left, right)
  numbersep=5pt,                   % how far the line-numbers are from the code
  numberstyle=\tiny\color{mygray}, % the style that is used for the line-numbers
  rulecolor=\color{black},         % if not set, the frame-color may be changed on line-breaks within not-black text (e.g. comments (green here))
  showspaces=false,                % show spaces everywhere adding particular underscores; it overrides 'showstringspaces'
  showstringspaces=false,          % underline spaces within strings only
  showtabs=false,                  % show tabs within strings adding particular underscores
  stepnumber=2,                    % the step between two line-numbers. If it's 1, each line will be numbered
  stringstyle=\color{mymauve},     % string literal style
  tabsize=2,                       % sets default tabsize to 2 spaces
  title=\lstname                   % show the filename of files included with \lstinputlisting; also try caption instead of title
}

\lstdefinestyle{customc}{
  belowcaptionskip=1\baselineskip,
  breaklines=true,
  frame=L,
  xleftmargin=\parindent,
  language=C,
  showstringspaces=false,
  basicstyle=\footnotesize\ttfamily,
  keywordstyle=\bfseries\color{green!40!black},
  commentstyle=\itshape\color{purple!40!black},
  identifierstyle=\color{blue},
  stringstyle=\color{orange},
}

\lstdefinestyle{customasm}{
  belowcaptionskip=1\baselineskip,
  frame=L,
  xleftmargin=\parindent,
  language=[x86masm]Assembler,
  basicstyle=\footnotesize\ttfamily,
  commentstyle=\itshape\color{purple!40!black},
}

\lstset{escapechar=@,style=customc, numberstyle=\footnotesize, basicstyle=\ttfamily\footnotesize}

\newsavebox{\riddlebox}
\newenvironment{colboxTre}[1]
{\newcommand\colboxcolor{#1}%
\begin{lrbox}{\riddlebox}%
\begin{minipage}{\dimexpr\columnwidth-2\fboxsep\relax} }
{\end{minipage}\end{lrbox}%
\begin{center}
  \colorbox[HTML]{\colboxcolor}{\usebox{\riddlebox}}
\end{center}}

\title{Analyzing and Predicting \texttt{LBUG}'s with a Hidden Markov Model}
\author{Thomas Stibor\newline\href{mailto:t.stibor@gsi.de}{t.stibor@gsi.de}}
\institute{High Performance Computing \newline GSI Helmholtz Centre for Heavy Ion Research \newline Darmstadt, Germany}
\date{Monday $22^\text{\scriptsize nd}$ September, 2014 \newline \newline 
\textcolor{red}{\textbf{LAD}'14} \textcolor{gray}{Workshop, Reims, France}}
\begin{document}
\lstset{language=C}
\maketitle
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}[fragile]\frametitle{Overview}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
 \begin{itemize}
  \item \texttt{LBUG}'s in Lustre and implications.
  \item Process Lustre log data with log stash.
  \item Markov Model (MM).
  \item Hidden Markov Model (HMM).
  \item Example (from text processing) what HMM's can analyze.
  \item Frequency distributions of function calls.
  \item Visualized MM transition and HMM emission matrices.
  \item Sampling and predicting LBUG's in future time windows.
\end{itemize}
\end{frame}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}[fragile]\frametitle{\texttt{LBUG}'s and Implications}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{itemize}
  \item Lustre is a large project with complex code base.
  \item Lustre is prone to \textcolor{red}{critical} software bugs called (\texttt{LBUG}'s).
  \item \texttt{LBUG} is a software behavior that causes freeze of kernel thread and subsequent reboot.
\end{itemize}
\begin{lstlisting}[frame=single,basicstyle=\LSTfont]
void lbug_with_loc(struct libcfs_debug_msg_data *) __attribute__((noreturn));

#define LBUG()                                                          \
do {                                                                    \
        LIBCFS_DEBUG_MSG_DATA_DECL(msgdata, D_EMERG, NULL);             \
        lbug_with_loc(&msgdata);                                        \
} while(0)

#define LIBCFS_DEBUG_MSG_DATA_DECL(dataname, mask, cdls)    \
        static struct libcfs_debug_msg_data dataname = {    \
               .msg_subsys = DEBUG_SUBSYSTEM,               \
               .msg_file   = __FILE__,                      \
               .msg_fn     = __FUNCTION__,                  \
               .msg_line   = __LINE__,                      \
               .msg_cdls   = (cdls)         };              \
        dataname.msg_mask   = (mask);
\end{lstlisting}
\vspace{-1.25cm}
\begin{quotation}
  ``Note - LBUG freezes the thread to allow capture of the panic stack. A system reboot is needed to clear the thread.''
\end{quotation}
{\tiny From:~\href{http://wiki.lustre.org/manual/LustreManual20_HTML/LustreTroubleshooting.html}{http://wiki.lustre.org/manual/LustreManual20\_HTML/LustreTroubleshooting.html}}
\vspace{0.3cm}
\end{frame}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}[fragile]\frametitle{\texttt{LBUG}'s in Lustre Code Example}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{lstlisting}[frame=single,basicstyle=\LSTfont]
int mdt_getxattr(struct mdt_thread_info *info)
{
        struct ptlrpc_request  *req = mdt_info_req(info);
        struct mdt_export_data *med = mdt_req2med(req);
        struct lu_ucred        *uc  = lu_ucred(info->mti_env);
        ...
        ...
        valid = info->mti_body->valid & (OBD_MD_FLXATTR | OBD_MD_FLXATTRLS);

        if (valid == OBD_MD_FLXATTR) {
                char *xattr_name = req_capsule_client_get(info->mti_pill,
                                                          &RMF_NAME);
                rc = mdt_getxattr_one(info, xattr_name, next, buf, med, uc);
        } else if (valid == OBD_MD_FLXATTRLS) {
                CDEBUG(D_INODE, "listxattr\n");

                rc = mo_xattr_list(info->mti_env, next, buf);
                if (rc < 0)
                        CDEBUG(D_INFO, "listxattr failed: %d\n", rc);
        } else if (valid == OBD_MD_FLXATTRALL) {
                rc = mdt_getxattr_all(info, reqbody, repbody,
                                      buf, next);
        } else
                LBUG();   
\end{lstlisting}
\end{frame}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}[fragile]\frametitle{Lustre Log Data Pre-Processing Steps}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{enumerate}
\item Fetching entire log data from archive tape.
\item Resulting in ``giant'' log data.
\end{enumerate}
\begin{lstlisting}[frame=single,basicstyle=\LSTfont]
drwxr-sr-x 14 root staff  114 Mai  2 11:37 ..
-rw-r-----  1 root adm   2,5G Feb  2  2012 syslog-20120201
-rw-r-----  1 root adm   2,0G Feb  3  2012 syslog-20120202
-rw-r-----  1 root adm   2,2G Feb  4  2012 syslog-20120203
-rw-r-----  1 root adm   2,0G Feb  5  2012 syslog-20120204
-rw-r-----  1 root adm   1,9G Feb  6  2012 syslog-20120205
-rw-r-----  1 root adm   1,9G Feb  7  2012 syslog-20120206
(...)

du -sh 2012/02/
57G     02/
\end{lstlisting}
\begin{itemize}
\item Total amount of Lustre log data for $2012$ is $\approx 2.1$ GByte. 
\item A true \textcolor{red}{Big Data} problem.
\end{itemize}
\end{frame}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}[fragile]\frametitle{Processed Lustre Logs}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
Logstash: CSV exported data to be analyzed:\\
\underline{Example 1:}
\begin{lstlisting}[frame=single,basicstyle=\LSTfont]
Mar 26 20:05:28,lxfs290,LustreError,filter.c,2732,__filter_oa2dentry,testlust-OST001f: filter_preprw_read on non-existent object: 10
Mar 26 20:05:28,lxfs290,LustreError,filter_io.c,488,filter_preprw_read,ASSERTION(PageLocked(lnb->page)) failed
Mar 26 20:05:28,lxfs290,LustreError,filter_io.c,488,filter_preprw_read,LBUG
\end{lstlisting}
\vspace{-4ex}
\underline{Example 2:}
\begin{lstlisting}[frame=single,basicstyle=\LSTfont]
May 27 22:56:01,lxfs124,LustreError,events.c,381,server_bulk_callback,"event type 4, status -5, desc ffff8800c791c000"
May 28 00:15:50,lxmds11,LustreError,client.c,178,ptlrpc_free_bulk,ASSERTION(atomic_read(&(desc->bd_export)->exp_refcount) < 0x5a5a5a) failed
May 28 00:15:50,lxmds11,LustreError,service.c,1426,ptlrpc_server_handle_request,ASSERTION(atomic_read(&(export)->exp_refcount) < 0x5a5a5a) failed
May 28 00:15:50,lxmds11,LustreError,service.c,1426,ptlrpc_server_handle_request,LBUG
May 28 00:15:50,lxmds11,LustreError,client.c,178,ptlrpc_free_bulk,LBUG
\end{lstlisting}
\vspace{-6ex}
\underline{Note: Patch exists for Example 2}
\vspace{-2ex}
\begin{figure}
  \includegraphics[width=11cm]{images/lbug.eps}
\end{figure}
\end{frame}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}[fragile]\frametitle{Can we predict \texttt{LBUG}'s in Lustre?}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
Consider this simple approach for modeling Lustre \emph{function calls} and corresponding \texttt{LBUG} occurrence:
\begin{figure}
  \centering
  \scalebox{0.8}{
    \begin{pspicture}[showgrid=false](0,0)(14,4)
      \rput(0,3){\rnode{1:fnc}{\texttt{mdt\_open}}}
      \rput(4,3){\rnode{2:fnc}{\texttt{mdt\_fseek}}}
      \rput(8,3){\rnode{3:fnc}{\texttt{mdt\_fstat}}}
      \rput(12,3){\rnode{4:fnc}{\texttt{mdt\_ost\_fid}}}
      \rput(0,0){\rnode{1:lbug}{\texttt{LBUG}=NO}}
      \rput(4,0){\rnode{2:lbug}{\texttt{LBUG}=NO}}
      \rput(8,0){\rnode{3:lbug}{\texttt{LBUG}=NO}}
      \rput(12,0){\rnode{4:lbug}{\texttt{LBUG}=YES}}
      \ncline[nodesep=3pt]{->}{1:fnc}{2:fnc}
      \ncline[nodesep=3pt]{->}{2:fnc}{3:fnc}
      \ncline[nodesep=3pt]{->}{3:fnc}{4:fnc}
      \ncline[nodesep=3pt]{->}{1:fnc}{1:lbug}
      \ncline[nodesep=3pt]{->}{2:fnc}{2:lbug}
      \ncline[nodesep=3pt]{->}{3:fnc}{3:lbug}
      \ncline[nodesep=3pt]{->}{4:fnc}{4:lbug}
    \end{pspicture}
    }
\end{figure}
\textcolor{blue}{This looks like the familiarized Hidden Markov model.}
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}[fragile]\frametitle{Markov Model Weather Example}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{figure}
  \centering
  \scalebox{1.0}{
    \begin{pspicture}[showgrid=false](0,0)(12,4)
      \cnodeput(1.5,2){0:rainy}{\makebox(0, 0.7){rainy}}
      \cnodeput(4.5,2){1:sunny}{\makebox(0, 0.7){sunny}}
      \cnodeput(3,4){start}{\makebox(0, 0.7){start}}

      \psset{linewidth=0.3pt,arrowsize=4pt}
      \ncline{->}{start}{0:rainy}\rput(2.1,3.3){$0.6$}
      \ncline{->}{start}{1:sunny}\rput(3.9,3.3){$0.4$}

      \ncarc[arcangle=15]{->}{0:rainy}{1:sunny}\rput(3,1.95){$0.4$}
      \ncarc[arcangle=15]{->}{1:sunny}{0:rainy}\rput(3,2.45){$0.3$}
      \nccircle[angle=45]{->}{0:rainy}{12.5pt}\rput(0.5,2.6){$0.7$}
      \nccircle[angle=-45]{<-}{1:sunny}{12.5pt}\rput(5.5,2.6){$0.6$}

      \psframe[linestyle=solid,fillstyle=solid](-0.5,2)(-0.3,2.7)
      \psframe[linestyle=solid,fillstyle=solid](-0.2,2)(0,2.3)

      \psframe[linestyle=solid,fillstyle=solid](6,2)(6.2,2.4)
      \psframe[linestyle=solid,fillstyle=solid](6.3,2)(6.5,2.6)

      \psframe[linestyle=solid,fillstyle=solid](2.75,2.8)(2.95,3.4)
      \psframe[linestyle=solid,fillstyle=solid](3.05,2.8)(3.25,3.2)

      \rput(8.7,3.6){\scriptsize rainy \hspace{0.1ex} sunny}
      \rput(8.4,3.3){$\mathbf{s} = \left(\, 0.6\, \, , \, \, 0.4 \, \right)$}

      \rput(9.8,2.85){\scriptsize rainy \hspace{1ex} sunny}
      \rput(8.4, 2.55){\scriptsize rainy}
      \rput(8.3, 2.1){\scriptsize sunny}
      \rput(9,2.3){$\mathbf{H} = \; \; \; \; \; \; \; \; \left(
          \begin{array}{ll}
            0.7 & 0.3 \\
            0.4 & 0.6
          \end{array}\right)
        $}
    \end{pspicture}
  }
\end{figure}
\vspace{-2cm}
\begin{table}
  {\scriptsize
  \begin{tabular}{lll}
    $T$ & : & Length of observation sequence. \\
    $N_{Q}$ & : & Number of states in the model. \\
    $\mathbf{s}$ & : & Initial state distribution. \\
    $\mathbf{H}$ & : & Transition matrix. \\
    $\{Q_{1},Q_{2},\ldots,Q_{T}\}$ & : & Set of time indexed random variables for states. \\
    $\mathfrak{M} = (\mathbf{s}, \mathbf{H})$ & : & Markov model parameters.
  \end{tabular}}
  %\caption{Notation table for Markov model.}
  \label{tab:notation.mm}
\end{table}
{\scriptsize
Joint distribution for \emph{sequence} of $T$ observations $\text{Pr}(Q_{1}, Q_{2}, \ldots, Q_{T})  = \text{Pr}(Q_{1}) \prod_{t=2}^{T} \text{Pr}(Q_{t} | Q_{1}, \ldots, Q_{t-1})$
\begin{colboxTre}{F8E0E0}
Joint distribution under \emph{first-order} Markov assumption:\\ $\text{Pr}(Q_{1}, Q_{2}, \ldots, Q_{T})  = \text{Pr}(Q_{1}) \prod_{t=2}^{T} \text{Pr}(Q_{t} | Q_{t-1})$
\end{colboxTre}
}
\end{frame}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}[fragile]\frametitle{Markov Model Weather Example (cont.)}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
Given that weather on day $1$ $(t=1)$ is sunny.
\begin{itemize}
\item What is the probability for the next $3$ days weather will be $\mathcal{O} = $``sunny$\rightarrow$rainy$\rightarrow$rainy''?
\end{itemize}
\begin{eqnarray*}
  \text{Pr}(\mathcal{O} | \mathfrak{M}) & = & \text{Pr}(Q_{1} = \text{sunny}, Q_{2} = \text{sunny}, Q_{3} = \text{rainy}, Q_{4} = \text{rainy}) \\
  & = & \text{Pr}(Q_{1} = \text{sunny}) \cdot \text{Pr}(Q_{2} = \text{sunny} \, | \, Q_{1} = \text{sunny}) \\
  & & \cdot \, \text{Pr}(Q_{3} = \text{rainy} \, | \, Q_{2} = \text{sunny}) \\ 
  & & \cdot \, \text{Pr}(Q_{4} = \text{rainy} \, | \, Q_{3} = \text{rainy}) \\
  & = & \mathbf{s}_{2} \cdot \mathbf{H}_{22} \cdot \mathbf{H}_{21} \cdot \mathbf{H}_{11} \\
  & = & 0.4 \cdot 0.6 \cdot 0.4 \cdot 0.7 = 0.0672 
\end{eqnarray*}
Note: Entries in matrix $\mathbf{H}$ can be interpreted as follows:
\begin{eqnarray*}
  \mathbf{H}_{ij} & = & \text{Pr}(Q_{t+1} = j \, | \, Q_{t} = i)
\end{eqnarray*}
\vspace{0.5cm}
where for the sake of simplicity states are from set $\{1,2,\ldots,N_{Q}\}$
\end{frame}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}[fragile]\frametitle{Hidden Markov models}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
Suppose we are locked in room without windows, and somebody is telling us the following
observations and ask us to tell him what weather is outside:
\begin{displaymath}
  \mathcal{O} = \text{walk} \rightarrow \text{clean} \rightarrow \text{shop} \rightarrow \cdots \rightarrow
  \text{shop}
\end{displaymath}
\begin{figure}
  \centering
  \scalebox{1.0}{
    \begin{pspicture}[showgrid=false](0,0)(12,4)
      \cnodeput[fillstyle=solid,fillcolor=lightgray](0,0){0:walk}{\makebox(0, 0.7){walk}}
      \cnodeput[fillstyle=solid,fillcolor=lightgray](3,0){1:shop}{\makebox(0, 0.7){shop}}
      \cnodeput[fillstyle=solid,fillcolor=lightgray](6,0){2:clean}{\makebox(0, 0.7){clean}}
      \cnodeput(1.5,2){0:rainy}{\makebox(0, 0.7){rainy}}
      \cnodeput(4.5,2){1:sunny}{\makebox(0, 0.7){sunny}}
      \cnodeput(3,4){start}{\makebox(0, 0.7){start}}

      \psset{linewidth=0.3pt,arrowsize=4pt}
      \ncline{->}{start}{0:rainy}\rput(2.1,3.3){$0.6$}
      \ncline{->}{start}{1:sunny}\rput(3.9,3.3){$0.4$}

      \ncarc[arcangle=15]{->}{0:rainy}{1:sunny}\rput(3,1.95){$0.4$}
      \ncarc[arcangle=15]{->}{1:sunny}{0:rainy}\rput(3,2.45){$0.3$}
      \nccircle[angle=45]{->}{0:rainy}{12.5pt}\rput(0.5,2.6){$0.7$}
      \nccircle[angle=-45]{<-}{1:sunny}{12.5pt}\rput(5.5,2.6){$0.6$}

      \ncline{->}{0:rainy}{0:walk}\rput(0.6,1.35){$0.1$}
      \ncline{->}{0:rainy}{1:shop}\rput(1.65,1.35){$0.4$}
      \ncline{->}{0:rainy}{2:clean}\rput(2.4,1.35){$0.5$}

      \ncline{->}{1:sunny}{0:walk}\rput(3.55,1.35){$0.7$} 
      \ncline{->}{1:sunny}{1:shop}\rput(4.3,1.35){$0.2$}
      \ncline{->}{1:sunny}{2:clean}\rput(5.4,1.35){$0.1$}

      \psframe[linestyle=solid,fillstyle=solid,fillcolor=lightgray](-0.8,1.2)(-0.6,1.3)
      \psframe[linestyle=solid,fillstyle=solid,fillcolor=lightgray](-0.5,1.2)(-0.3,1.6)
      \psframe[linestyle=solid,fillstyle=solid,fillcolor=lightgray](-0.2,1.2)(0,1.7)

      \psframe[linestyle=solid,fillstyle=solid,fillcolor=lightgray](6,1.2)(6.2,1.9)
      \psframe[linestyle=solid,fillstyle=solid,fillcolor=lightgray](6.3,1.2)(6.5,1.4)
      \psframe[linestyle=solid,fillstyle=solid,fillcolor=lightgray](6.6,1.2)(6.8,1.3)

      \psframe[linestyle=solid,fillstyle=solid](-0.5,2)(-0.3,2.7)
      \psframe[linestyle=solid,fillstyle=solid](-0.2,2)(0,2.3)

      \psframe[linestyle=solid,fillstyle=solid](6,2)(6.2,2.4)
      \psframe[linestyle=solid,fillstyle=solid](6.3,2)(6.5,2.6)

      \psframe[linestyle=solid,fillstyle=solid](2.75,2.8)(2.95,3.4)
      \psframe[linestyle=solid,fillstyle=solid](3.05,2.8)(3.25,3.2)

      \rput(8.7,3.6){\scriptsize rainy \hspace{0.1ex} sunny}
      \rput(8.4,3.3){$\mathbf{s} = \left(\, 0.6\, \, , \, \, 0.4 \, \right)$}

      \rput(9.8,2.85){\scriptsize rainy \hspace{1ex} sunny}
      \rput(8.4, 2.55){\scriptsize rainy}
      \rput(8.3, 2.1){\scriptsize sunny}
      \rput(9,2.3){$\mathbf{H} = \; \; \; \; \; \; \; \; \left(
          \begin{array}{ll}
            0.7 & 0.3 \\
            0.4 & 0.6
          \end{array}\right)
        $}
      \rput(10.1,1.3){\scriptsize walk \hspace{1ex} shop \hspace{1ex} clean}
      \rput(8.4, 1.05){\scriptsize rainy}
      \rput(8.3, 0.6){\scriptsize sunny}
      \rput(9.4,  0.8){$\mathbf{E} = \qquad \left(
          \begin{array}{lll}
            0.1 & 0.4 & 0.5 \\
            0.7 & 0.2 & 0.1
          \end{array}\right)
        $}
    \end{pspicture}
  }
\end{figure}
\end{frame}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}[fragile]\frametitle{Hidden Markov models (cont.)}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% \begin{table}
%   {\scriptsize
%   \begin{tabular}{lll}
%     $T$ & : & Length of observation sequence. \\
%     $N_{Q}$ & : & Number of states in the model. \\
%     $\mathbf{s}$ & : & Initial state distribution. \\
%     $\mathbf{H}$ & : & Transition matrix. \\
%     $\mathbf{E}$ & : & Emission probability matrix. \\
%     $\{Q_{1},Q_{2},\ldots,Q_{T}\}$ & : & Set of time indexed random variables for states. \\
%     $\{O_{1},O_{2},\ldots,O_{T}\}$ & : & Set of time indexed random variables for observable symbols. \\
%     $\mathbf{q} = (q_{1}, q_{2}, \ldots, q_{T})$  & : & Sequence of time indexed given hidden state variables. \\
%     $\mathbf{o} = (o_{1}, o_{2}, \ldots, o_{T})$  & : & Sequence of time indexed given observed symbol variables. \\
%     $\mathfrak{M} = (\mathbf{s}, \mathbf{H}, \mathbf{E})$ & : & Hidden Markov model parameters.
%   \end{tabular}}
%   %\caption{Notation table for Markov model.}
%   \label{tab:notation.mm}
% \end{table}
\underline{Problem 1:} Calculate probability of observation sequence $\mathcal{O}$, given model $\mathfrak{M}$, that is
$\text{Pr}(\mathcal{O} \, | \, \mathfrak{M}) = ?$\\[2ex]
\underline{Problem 2:} Given HMM and observation sequence $\mathcal{O}$, find most likely hidden state sequence.\\[2ex]
\underline{Problem 3:} How do we estimate model parameters $\mathfrak{M} = (\mathbf{s}, \mathbf{H}, \mathbf{E})$ to maximize $\text{Pr}(\mathcal{O} \, | \, \mathfrak{M})$. Loosely speaking, how do we
estimate $\mathfrak{M}$ that ``best'' fits our data $\mathcal{O}$.\\[2ex]
For further details see paper: 
\begin{itemize}
\item \emph{Lawrence R. Rabiner}: A Tutorial on Hidden Markov Models and Selected Applications in Speech Recognition. Proceedings of the IEEE, 1989, pages 257-286.
\end{itemize}
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}[fragile]\frametitle{Analyze Novel Flatland with a HMM}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
Example from Sec. 1 \emph{Of the Nature of Flatland:} by Edwin A. Abbott
\begin{lstlisting}[frame=single,basicstyle=\LSTfont]
I call our world Flatland, not because we call it so, but to make its
nature clearer to you, my happy readers, who are privileged to live in
Space.

Imagine a vast sheet of paper on which straight Lines, Triangles,
Squares, Pentagons, Hexagons, and other figures, ...
\end{lstlisting}
\vspace{-5ex}
Process and convert text into:
\begin{lstlisting}[frame=single,basicstyle=\LSTfont]
i c a l l o u r w o r l d f l a t l a n d n o t b e c a u s e w e c a l l i t
s o b u t t o m a k e i t s n a t u r e c l e a r e r t o y o u m y h a p p y
r e a d e r s w h o a r e p r i v i l e g e d t o l i v e i n s p a c e i m a
g i n e a v a s t s h e e t o f p a p e r o n w h i c h s t r a i g h t l i n
e s t r i a n g l e s s q u a r e s p e n t a g o n s h e x a g o n s a n d o
t h e r f i g u r e s
\end{lstlisting}
\vspace{-5ex}
Train HMM of following form:
\begin{figure}
  \centering
  \scalebox{0.5}{
    \begin{pspicture}[showgrid=false](0,0)(12,4)
      \cnodeput[fillstyle=solid,fillcolor=lightgray](0,0){1:a}{\makebox(0, 0.7){a}}
      \cnodeput[fillstyle=solid,fillcolor=lightgray](1.5,0){2:b}{\makebox(0, 0.7){b}}
      \cnodeput[fillstyle=solid,fillcolor=lightgray](3,0){3:c}{\makebox(0, 0.7){c}}

      \rput(4.5, 0){\Huge $\ldots$}

      \cnodeput[fillstyle=solid,fillcolor=lightgray](6,0){24:x}{\makebox(0, 0.7){x}}
      \cnodeput[fillstyle=solid,fillcolor=lightgray](7.5,0){25:y}{\makebox(0, 0.7){y}}
      \cnodeput[fillstyle=solid,fillcolor=lightgray](9,0){26:z}{\makebox(0, 0.7){z}}

      \cnodeput(3,2){1:hidden}{\makebox(0, 0.7){$h_{1}$}}
      \cnodeput(6,2){2:hidden}{\makebox(0, 0.7){$h_{2}$}}
      \cnodeput(4.5,4){start}{\makebox(0, 0.7){start}}

      \psset{linewidth=0.3pt,arrowsize=4pt}
      \ncline{->}{start}{1:hidden}
      \ncline{->}{start}{2:hidden}

      \ncarc[arcangle=15]{->}{1:hidden}{2:hidden}
      \ncarc[arcangle=15]{->}{2:hidden}{1:hidden}
      \nccircle[angle=45]{->}{1:hidden}{12.5pt}
      \nccircle[angle=-45]{<-}{2:hidden}{12.5pt}

      \ncline{->}{1:hidden}{1:a}
      \ncline{->}{1:hidden}{2:b}
      \ncline{->}{1:hidden}{3:c}
      \ncline{->}{1:hidden}{24:x}
      \ncline{->}{1:hidden}{25:y}
      \ncline{->}{1:hidden}{26:z}

      \ncline{->}{2:hidden}{1:a}
      \ncline{->}{2:hidden}{2:b}
      \ncline{->}{2:hidden}{3:c}
      \ncline{->}{2:hidden}{24:x}
      \ncline{->}{2:hidden}{25:y}
      \ncline{->}{2:hidden}{26:z}
    \end{pspicture}
  }
\end{figure}
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}[fragile]\frametitle{\qquad \qquad \hspace{4ex} Analyze Novel Flatland with a HMM (cont.)}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{minipage}{0.4\linewidth}
\vspace{-6ex}
\begin{table}
  \hspace{-2.1cm}
  %\centering
  {\scriptsize
  \begin{tabular}{ccc}
    letter & $h_{1}$ & $h_{2}$ \\ \hline
    a      & $0.00$   & $16.51$ \\
    b      & $2.70$   & $0.00$ \\
    c      & $5.37$   & $0.24$ \\
    d      & $5.06$   & $2.39$ \\
    e      & $0.00$   & $27.10$ \\
    f      & $4.59$   & $0.00$ \\
    g      & $1.84$   & $1.92$ \\
    h      & $10.10$   & $0.00$ \\
    i      & $0.00$   & $16.14$ \\
    j      & $0.16$   & $0.00$ \\
    k      & $0.50$   & $0.16$ \\
    l      & $8.02$   & $0.00$ \\
    m      & $4.97$   & $0.00$ \\
    n      & $13.58$   & $0.00$ \\
    o      & $0.00$   & $17.03$ \\
    p      & $2.55$   & $0.77$ \\
    q      & $0.13$   & $0.13$ \\
    r      & $10.90$   & $0.00$ \\
    s      & $12.06$   & $0.30$ \\
    t      & $9.03$   & $9.29$ \\
    u      & $1.49$   & $4.80$ \\
    v      & $1.78$   & $0.00$ \\
    w      & $3.04$   & $0.34$ \\
    x      & $0.46$   & $0.00$ \\
    y      & $1.46$   & $2.82$ \\
    z      & $0.11$   & $0.00$ 
  \end{tabular}}
$\times 100$
\end{table}
\end{minipage}
\hspace{-2cm}
\begin{minipage}{0.5\linewidth}
%\vspace{-1cm}
\begin{figure}
  \centering
  \psfrag{h1}{$h_{1}$ \scriptsize{(vowels)}}
  \psfrag{h2}{$h_{2}$ \scriptsize{(conson.)}}
  \psfrag{a}{$a$}
  \psfrag{e}{$e$}
  \psfrag{i}{$i$}
  \psfrag{o}{$o$}
  \psfrag{u}{$u$}
  \includegraphics[width=9cm]{images/flatland.emission.matrix.eps}
\end{figure}
\end{minipage}
\end{frame}
% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% \begin{frame}[fragile]\frametitle{Hidden Markov models (cont.)}
% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Consider a state hidden sequence $\mathbf{Q} = Q_{1}, Q_{2}, \ldots, Q_{T}$ where $Q_{1}$ is initial
% state, we assume statistical independence of \textcolor{red}{observations} and thus get
% \begin{eqnarray*}
%   \text{Pr}(\mathbf{O} \, | \mathbf{Q}, \mathfrak{M}) & = & \prod_{t=1}^{T} \text{Pr}(O_{t} \, | \, Q_{t}, \mathfrak{M}) \\
%   & = & \mathbf{E}_{Q_{1},O_{1}} \cdot \mathbf{E}_{Q_{2},O_{2}} \cdots \mathbf{E}_{Q_{T},O_{T}}.
% \end{eqnarray*}
% Probability of hidden state sequence:
% \begin{eqnarray*}
%   \text{Pr}(\mathbf{Q} \, | \, \mathfrak{M}) & = & \text{Pr}(Q_{1}) \prod_{t=2}^{T}\text{Pr}(Q_{t} \, | Q_{t-1}) \\
%    & = & \mathbf{s}_{Q_{1}} \cdot \mathbf{H}_{Q_{1}Q_{2}} \cdots \mathbf{H}_{Q_{T-1}Q_{T}}.
% \end{eqnarray*}
% Then the joint probability distribution is
% \begin{eqnarray*}
%   \text{Pr}(\mathbf{O}, \mathbf{Q} \, | \, \mathfrak{M}) & = & \text{Pr}(Q_{1}) \prod_{t=2}^{T}\text{Pr}(Q_{t} \, | Q_{t-1}) \prod_{t=1}^{T} \text{Pr}(O_{t} \, | \, Q_{t})
% \end{eqnarray*}
% \end{frame}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}[fragile]\frametitle{Frequency Distribution of Function Calls (1-Gram Seq.)}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{figure}
  \centering
  \includegraphics[width=0.5\linewidth]{images/lxmds11-lustre-01-2013.eps}
  \includegraphics[width=0.5\linewidth]{images/lxmds11-lustre-02-2013.eps}
\end{figure}
\begin{figure}
  \centering
  \includegraphics[width=0.5\linewidth]{images/lxmds11-lustre-03-2013.eps}
  \includegraphics[width=0.5\linewidth]{images/lxmds11-lustre-04-2013.eps}
\end{figure}
\end{frame}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}[fragile]\frametitle{Frequency Distribution of Func. Calls (1-Gram Seq.) (cont.)}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{figure}
  \centering
  \includegraphics[width=0.5\linewidth]{images/lxmds11-lustre-05-2013.eps}
  \includegraphics[width=0.5\linewidth]{images/lxmds11-lustre-06-2013.eps}
\end{figure}
\begin{figure}
  \centering
  \includegraphics[width=0.5\linewidth]{images/lxmds11-lustre-07-2013.eps}
  \includegraphics[width=0.5\linewidth]{images/lxmds11-lustre-08-2013.eps}
\end{figure}
\end{frame}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}[fragile]\frametitle{Visualize Markov Model (2-Gram Sequence) of Fnc. Calls}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\vspace{-0.5cm}
\begin{figure}
  \centering
  \includegraphics[width=0.9\linewidth]{images/lxmds11-circle-01-2013.eps}
\end{figure}
\end{frame}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}[fragile]\frametitle{Visualize Markov M. (2-Gram Seq.) of Fnc. Calls (cont.)}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\vspace{-0.5cm}
\begin{figure}
  \centering
  \includegraphics[width=0.9\linewidth]{images/lxmds11-circle-02-2013.eps}
\end{figure}
\end{frame}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}[fragile]\frametitle{Visualize Markov M. (2-Gram Seq.) of Fnc. Calls (cont.)}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\vspace{-0.5cm}
\begin{figure}
  \centering
  \includegraphics[width=0.9\linewidth]{images/lxmds11-circle-03-2013.eps}
\end{figure}
\end{frame}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}[fragile]\frametitle{Visualize Markov M. (2-Gram Seq.) of Fnc. Calls (cont.)}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\vspace{-0.5cm}
\begin{figure}
  \centering
  \includegraphics[width=0.9\linewidth]{images/lxmds11-circle-04-2013.eps}
\end{figure}
\end{frame}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}[fragile]\frametitle{Visualize Markov M. (2-Gram Seq.) of Fnc. Calls (cont.)}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\vspace{-0.5cm}
\begin{figure}
  \centering
  \includegraphics[width=0.9\linewidth]{images/lxmds11-circle-05-2013.eps}
\end{figure}
\end{frame}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}[fragile]\frametitle{Visualize Markov M. (2-Gram Seq.) of Fnc. Calls (cont.)}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\vspace{-0.5cm}
\begin{figure}
  \centering
  \includegraphics[width=0.9\linewidth]{images/lxmds11-circle-06-2013.eps}
\end{figure}
\end{frame}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}[fragile]\frametitle{Visualize Markov M. (2-Gram Seq.) of Fnc. Calls (cont.)}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\vspace{-0.5cm}
\begin{figure}
  \centering
  \includegraphics[width=0.9\linewidth]{images/lxmds11-circle-07-2013.eps}
\end{figure}
\end{frame}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}[fragile]\frametitle{Visualize Markov M. (2-Gram Seq.) of Fnc. Calls (cont.)}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\vspace{-0.5cm}
\begin{figure}
  \centering
  \includegraphics[width=0.9\linewidth]{images/lxmds11-circle-08-2013.eps}
\end{figure}
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}[fragile]\frametitle{Visualize HMM  of Functions Calls}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\vspace{-0.5cm}
\begin{figure}
  \centering
  \includegraphics[width=0.9\linewidth]{images/hmm_h2.RData.eps}
\end{figure}
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}[fragile]\frametitle{Visualize HMM  of Functions Calls (cont.)}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\vspace{-0.5cm}
\begin{figure}
  \centering
  \includegraphics[width=0.9\linewidth]{images/hmm_h3.RData.eps}
\end{figure}
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}[fragile]\frametitle{Visualize HMM  of Functions Calls (cont.)}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\vspace{-0.5cm}
\begin{figure}
  \centering
  \includegraphics[width=0.9\linewidth]{images/hmm_h4.RData.eps}
\end{figure}
\end{frame}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}[fragile]\frametitle{\texttt{LBUG} Prediction with HMM}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{itemize}
\item Train HMM on Lustre logs (function calls, \texttt{LBUG}s)
\item Sample function call sequences from HMM.
\end{itemize}
Example: Sample from trained HMM (2 hidden states, 36 emitting states (function calls))
\begin{lstlisting}[frame=single,basicstyle=\LSTfont]
ldlm_handle_enqueue, lov_find_pool, target_handle_connect,
target_handle_reconnect, target_handle_reconnect, target_handle_reconnect,
lov_find_pool, target_send_reply_msg, mds_close,
lov_clear_orphans, ...., target_handle_reconnect, target_handle_reconnect,
target_handle_reconnect, LBUG
\end{lstlisting}
%Expected number of \texttt{LBUG}'s within the next $T$ time steps.
\vspace{-1cm}
\begin{figure}
  \centering
  \includegraphics[width=0.4\linewidth]{images/expected.num.lbugs.2.eps}
\end{figure}
\end{frame}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{frame}[fragile]\frametitle{Summary \& Outlook}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
Analyzing Lustre log files with (Hidden) Markov Models for:
\begin{itemize}
\item visualizing and analyzing problems,
\item recover latent structures and relations,
\item predicting future problems (\texttt{LBUG}'s), by sampling from the model.
\end{itemize}
HMM implementation (in \texttt{C}) and \texttt{R} scripts (for generating chord diagrams)
will be available soon at \href{https://bitbucket.org/tstibor}{https://bitbucket.org/tstibor}\\
\begin{center}
  Many thanks to \textcolor{blue}{Matteo Dessalvi} for providing Lustre log data and great discussions on machine learning.
\end{center}
\begin{center}
  \huge{Questions ?}
\end{center}
\end{frame}
\end{document}
